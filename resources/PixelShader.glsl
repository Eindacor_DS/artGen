#version 440

out vec4 output_color;
in vec4 fragment_color;
uniform vec4 test;

void main()
{
	output_color = fragment_color;
}