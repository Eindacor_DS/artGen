#include "Camera.h"

Camera::Camera(const shared_ptr<ogl_context>& _context, const shared_ptr<InputTracker>& _inputTracker)
{
	setDefaults();

	context = _context;
	inputTracker = _inputTracker;
	camera = shared_ptr<ogl_camera_flying> (new ogl_camera_flying(inputTracker->getKeyHandler(), context, vec3(0.0f, eyeLevel, 10.0f), 45.0f));

	camera->setStepDistance(0.02f);
	camera->setStrafeDistance(0.02f);
	camera->setRotateAngle(1.0f);
	camera->setTiltAngle(1.0f);
}

void Camera::update(const shared_ptr<InputTracker>& inputTracker)
{
	camera->updateCamera();
	vec3 camera_pos = camera->getPosition();
	context->setUniform3fv("camera_position", 1, camera_pos);
	camera->setMVP(context, mat4(1.0f), jep::NORMAL);

	if (inputTracker->isPressed(GLFW_KEY_HOME, false) && inputTracker->shiftIsHeld())
	{
		depthOfFieldPasses = glm::clamp(depthOfFieldPasses + 1, 1, 50);
		cout << "dof_passes: " << depthOfFieldPasses << endl;
	}

	if (inputTracker->isPressed(GLFW_KEY_END, false) && inputTracker->shiftIsHeld())
	{
		depthOfFieldPasses = glm::clamp(depthOfFieldPasses - 1, 1, 50);
		cout << "dof_passes: " << depthOfFieldPasses << endl;
	}

	if (inputTracker->isPressed(GLFW_KEY_PAGE_UP, true))
	{
		aperture = glm::clamp(aperture + 0.001f, 0.001f, 0.5f);
		cout << "dof_aperture: " << depthOfFieldPasses << endl;
	}

	if (inputTracker->isPressed(GLFW_KEY_PAGE_DOWN, true))
	{
		aperture = glm::clamp(aperture - 0.001f, 0.001f, 0.5f);
		cout << "dof_aperture: " << aperture << endl;
	}

	if (inputTracker->isPressed(GLFW_KEY_F12, false))
	{
		depthOfFieldEnabled = !depthOfFieldEnabled;
	}

	if (inputTracker->isPressed(GLFW_KEY_HOME, true) && !inputTracker->shiftIsHeld())
	{
		camera->adjustFocalLength(1.1f);
		std::cout << "focal length: " << glm::length(camera->getFocus() - camera->getPosition()) << endl;
	}

	if (inputTracker->isPressed(GLFW_KEY_END, true) && !inputTracker->shiftIsHeld())
	{
		camera->adjustFocalLength(0.9f);
		std::cout << "focal length: " << glm::length(camera->getFocus() - camera->getPosition()) << endl;
	}

	if (inputTracker->isPressed(GLFW_KEY_9, false))
	{
		camera->setPosition(vec3(0.0f, eyeLevel, 5.0f));
		camera->setFocus(vec3(0.0f, 0.0f, 0.0f));
	}

	if (inputTracker->isPressed(GLFW_KEY_INSERT, true))
	{
		fieldOfView = glm::clamp(fieldOfView + 0.5f, 1.0f, 180.0f);
		camera->setFOV(fieldOfView);
		std::cout << fieldOfView << endl;
	}

	if (inputTracker->isPressed(GLFW_KEY_DELETE, true))
	{
		fieldOfView = glm::clamp(fieldOfView - 0.5f, 1.0f, 180.0f);
		camera->setFOV(fieldOfView);
		std::cout << fieldOfView << endl;
	}
}

glm::mat4 Camera::getMVP()
{
	return glm::mat4();
}

vector<glm::mat4> Camera::getDOFMVPs()
{
	vector<glm::mat4> dofMVPs;

	vec3 camera_vector = glm::normalize(camera->getFocus() - camera->getPosition());
	vec3 camera_right = glm::normalize(glm::cross(vec3(camera_vector.x, 0.0f, camera_vector.z), vec3(0.0f, 1.0f, 0.0f)));
	vec3 camera_up = -1.0f * glm::normalize(glm::cross(camera_vector, camera_right));

	for (int i = 0; i < depthOfFieldPasses; i++)
	{
		vec3 bokeh = camera_right * cos((float)i * 2.0f * PI / (float)depthOfFieldPasses) + camera_up * sinf((float)i * 2.0f * PI / (float)depthOfFieldPasses);
		glm::mat4 modelview = glm::lookAt(camera->getPosition() + aperture * bokeh, camera->getFocus(), camera_up);
		glm::mat4 mvp = camera->getProjectionMatrix() * modelview;

		dofMVPs.push_back(mvp);
	}

	return dofMVPs;
}

void Camera::setDefaults()
{
	aperture = 0.005;
	depthOfFieldEnabled = false;
	depthOfFieldPasses = 10;
	fieldOfView = 45.0f;
	eyeLevel = 0.0f;
}
