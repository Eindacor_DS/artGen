#include "Renderer.h"

Renderer::Renderer(
	const shared_ptr<ogl_context>& _context, 
	const shared_ptr<Camera>& _camera,  
	const shared_ptr<ColorManager>& _colorManager,
	const shared_ptr<BufferManager>& _bufferManager,
	const shared_ptr<RandomGenerator>& _randomGenerator,
	const shared_ptr<GenerationProperties>& _settingsManager
)
{
	context = _context;
	camera = _camera;
	colorManager = _colorManager;
	bufferManager = _bufferManager;
	randomGenerator = _randomGenerator;
	generationProperties = _settingsManager;

	GLint range[2];
	glEnable(GL_PROGRAM_POINT_SIZE);
	glGetIntegerv(GL_ALIASED_POINT_SIZE_RANGE, range);
	maxPointSize = range[1] < 1 ? min(context->getWindowHeight(), context->getWindowWidth()) : min(min(context->getWindowHeight(), context->getWindowWidth()), range[1]);

	fractal_scale_matrix = glm::scale(mat4(1.0f), vec3(generationProperties->fractal_scale, generationProperties->fractal_scale, generationProperties->fractal_scale));

	glEnable(GL_DEPTH_CLAMP);
	glDepthRange(0.0, 1.0);
}

void Renderer::render(const shared_ptr<ArtForm>& artForm) const
{
	updateBackground(artForm);

	// bind target VAO
	glBindVertexArray(bufferManager->getVerticesVAO());
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);

	context->setUniform1i("invert_colors", generationProperties->inverted ? 1 : 0);
	context->setUniform1i("lighting_mode", generationProperties->lm);
	context->setUniform3fv("centerpoint", 1, artForm->getCentroid());
	context->setUniform1f("illumination_distance", generationProperties->lm == CAMERA ? generationProperties->illumination_distance * 10.0f : generationProperties->illumination_distance);
	context->setUniform1f("point_size_modifier", point_size_modifier);
	context->setUniform1i("max_point_size", maxPointSize);

	glBindBuffer(GL_ARRAY_BUFFER, bufferManager->getVerticesVBO());

	if (camera->dofIsEnabled())
	{
		drawWithDOF(artForm, camera);
	}
	else
	{
		draw(artForm);
	}

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
}

void Renderer::draw(const shared_ptr<ArtForm>& artForm) const
{
	int geometry_passes = 0;
	geometry_passes = int(generationProperties->enable_triangles && generationProperties->triangle_mode != 0) + int(generationProperties->enable_lines && generationProperties->line_mode != 0) + int(generationProperties->show_points);

	bool accum_loaded = false;

	if (generationProperties->show_points)
	{
		drawVertices(artForm);
		glAccum(accum_loaded ? GL_ACCUM : GL_LOAD, 1.0f / float(geometry_passes));
		accum_loaded = true;
	}

	if (generationProperties->enable_lines && generationProperties->line_mode != 0)
	{
		drawLines(artForm);
		glAccum(accum_loaded ? GL_ACCUM : GL_LOAD, 1.0f / float(geometry_passes));
		accum_loaded = true;
	}

	if (generationProperties->enable_triangles && generationProperties->triangle_mode != 0)
	{
		drawTriangles(artForm);
		glAccum(accum_loaded ? GL_ACCUM : GL_LOAD, 1.0f / float(geometry_passes));
		accum_loaded = true;
	}

	if (geometry_passes != 0)
	{
		glAccum(GL_RETURN, 1.0f / float(geometry_passes));
	}
}

void Renderer::drawWithDOF(const shared_ptr<ArtForm>& artForm, const shared_ptr<Camera> & camera) const
{
	int geometry_passes = int(generationProperties->enable_triangles && generationProperties->triangle_mode != 0) + int(generationProperties->enable_lines && generationProperties->line_mode != 0) + int(generationProperties->show_points);

	float total_passes = float(camera->getDOFPasses() * geometry_passes);
	bool accum_loaded = false;

	vector<glm::mat4> dofMVPs = camera->getDOFMVPs();

	for (const auto T_mat4 : dofMVPs)
	{
		context->setUniformMatrix4fv("MVP", 1, GL_FALSE, T_mat4);

		if (generationProperties->show_points)
		{
			drawVertices(artForm);
			glAccum(accum_loaded ? GL_ACCUM : GL_LOAD, 1.0f / total_passes);
			accum_loaded = true;
		}

		if (generationProperties->enable_lines && generationProperties->line_mode != 0)
		{
			drawLines(artForm);
			glAccum(accum_loaded ? GL_ACCUM : GL_LOAD, 1.0f / total_passes);
			accum_loaded = true;
		}

		if (generationProperties->enable_triangles && generationProperties->triangle_mode != 0)
		{
			drawTriangles(artForm);
			glAccum(accum_loaded ? GL_ACCUM : GL_LOAD, 1.0f / total_passes);
			accum_loaded = true;
		}
	}

	if (total_passes > 0.5f)
	{
		glAccum(GL_RETURN, 1.0f / total_passes);
	}
}

void Renderer::drawVertices(const shared_ptr<ArtForm>& artForm) const
{
	context->setUniform1i("geometry_type", 0);
	glDrawArrays(GL_POINTS, 0, artForm->getVerticesToRenderCount());
}

void Renderer::drawLines(const shared_ptr<ArtForm>& artForm) const
{
	context->setUniform1i("geometry_type", 1);
	if (generationProperties->line_mode == GL_LINES)
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferManager->getLineIndices());
		glDrawElements(generationProperties->line_mode, artForm->getLineVerticesToRenderCount(), GL_UNSIGNED_SHORT, (void*)0);
	}
	else
	{
		glDrawArrays(generationProperties->line_mode, 0, artForm->getVerticesToRenderCount());
	}
}

void Renderer::drawTriangles(const shared_ptr<ArtForm>& artForm) const
{
	context->setUniform1i("geometry_type", 2);
	if (generationProperties->triangle_mode == GL_TRIANGLES)
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferManager->getTriangleIndices());
		glDrawElements(generationProperties->triangle_mode, artForm->getTriangleVerticesToRenderCount(), GL_UNSIGNED_SHORT, (void*)0);
	}
	else
	{
		glDrawArrays(generationProperties->triangle_mode, 0, artForm->getVerticesToRenderCount());
	}
}

void Renderer::update(const shared_ptr<InputTracker>& inputTracker, const shared_ptr<ArtForm>& artForm)
{
	if (inputTracker->isPressed(GLFW_KEY_L, false) && generationProperties->enable_lines)
	{
		switch (generationProperties->line_mode)
		{
		case 0: generationProperties->line_mode = GL_LINES; break;
		case GL_LINES: generationProperties->line_mode = GL_LINE_STRIP; break;
		case GL_LINE_STRIP: generationProperties->line_mode = 0; break;
		default: break;
		}
	}

	if (inputTracker->isPressed(GLFW_KEY_K, false) && generationProperties->enable_triangles)
	{
		switch (generationProperties->triangle_mode)
		{
		case 0: generationProperties->triangle_mode = GL_TRIANGLES; break;
		case GL_TRIANGLES: generationProperties->triangle_mode = GL_TRIANGLE_STRIP; break;
		case GL_TRIANGLE_STRIP: generationProperties->triangle_mode = GL_TRIANGLE_FAN; break;
		case GL_TRIANGLE_FAN: generationProperties->triangle_mode = 0; break;
		default: break;
		}
	}

	if (inputTracker->isPressed(GLFW_KEY_M, false))
	{
		if (inputTracker->shiftIsHeld() && inputTracker->controlIsHeld())
		{
			cycleLightColorOverride(artForm);
		}
		else if (inputTracker->shiftIsHeld())
		{		
			cycleLineColorOverride(artForm);
		}
		else if (inputTracker->controlIsHeld())
		{		
			cycleTriangleColorOverride(artForm);
		}
		else if (inputTracker->altIsHeld())
		{
			cyclePointColorOverride(artForm);
		}
		else 
		{
			artForm->newColors();
			updatePointColorOverride(artForm);
			updateLineColorOverride(artForm);
			updateTriangleColorOverride(artForm);
		}
	}

	if (inputTracker->isPressed(GLFW_KEY_P, false))
	{
		generationProperties->show_points = !generationProperties->show_points;
	}

	if (inputTracker->isPressed(GLFW_KEY_1, false))
	{
		cycleBackgroundColorIndex(artForm);
	}

	if (inputTracker->isPressed(GLFW_KEY_2, false))
	{
		generationProperties->no_background = !generationProperties->no_background;
		updateBackground(artForm);
	}

	if (inputTracker->isPressed(GLFW_KEY_4, false))
	{
		generationProperties->light_effects_transparency = !generationProperties->light_effects_transparency;
		context->setUniform1i("light_effects_transparency", generationProperties->light_effects_transparency);
	}

	if (inputTracker->isPressed(GLFW_KEY_Q, false))
	{
		generationProperties->randomize_lightness = !generationProperties->randomize_lightness;
		if (generationProperties->randomize_lightness)
		{
			cout << "randomize lightness enabled" << endl;
		}
		else
		{
			cout << "randomize lightness disabled" << endl;
		}
	}

	if (inputTracker->isPressed(GLFW_KEY_U, false))
	{
		generationProperties->randomize_alpha = !generationProperties->randomize_alpha;
		if (generationProperties->randomize_alpha)
		{
			cout << "randomize alpha enabled" << endl;
		}
		else
		{
			cout << "randomize alpha disabled" << endl;
		}
	}

	if (inputTracker->isPressed(GLFW_KEY_PERIOD, false))
	{
		if (inputTracker->shiftIsHeld())
		{
			point_size_modifier = glm::clamp(point_size_modifier + 0.1f, 0.0f, 2.0f);
			context->setUniform1f("point_size_modifier", point_size_modifier);
		}
		else
		{
			adjustLineWidth(true);
		}
	}

	if (inputTracker->isPressed(GLFW_KEY_COMMA, false))
	{
		if (inputTracker->shiftIsHeld())
		{
			point_size_modifier = glm::clamp(point_size_modifier - 0.1f, 0.0f, 2.0f);
			context->setUniform1f("point_size_modifier", point_size_modifier);
		}

		else
		{
			adjustLineWidth(false);
		}
	}

	if (inputTracker->isPressed(GLFW_KEY_B, true))
	{
		generationProperties->fractal_scale *= 1.1f;
		fractal_scale_matrix = glm::scale(mat4(1.0f), vec3(generationProperties->fractal_scale, generationProperties->fractal_scale, generationProperties->fractal_scale));
		context->setUniformMatrix4fv("fractal_scale", 1, GL_FALSE, fractal_scale_matrix);
		cout << "scale: " << generationProperties->fractal_scale << endl;
	}

	if (inputTracker->isPressed(GLFW_KEY_V, true))
	{
		generationProperties->fractal_scale /= 1.1f;
		fractal_scale_matrix = glm::scale(mat4(1.0f), vec3(generationProperties->fractal_scale, generationProperties->fractal_scale, generationProperties->fractal_scale));
		context->setUniformMatrix4fv("fractal_scale", 1, GL_FALSE, fractal_scale_matrix);
		cout << "scale: " << generationProperties->fractal_scale << endl;
	}

	if (inputTracker->isPressed(GLFW_KEY_I, false))
	{
		invertColors(artForm);
	}

	if (inputTracker->isPressed(GLFW_KEY_Z, false))
	{
		generationProperties->smooth_render = !generationProperties->smooth_render;
	}

	if ((inputTracker->isPressed(GLFW_KEY_RIGHT_BRACKET, true) || inputTracker->isPressed(GLFW_KEY_LEFT_BRACKET, true)) && inputTracker->shiftIsHeld())
	{

		if (inputTracker->isPressed(GLFW_KEY_RIGHT_BRACKET, true))
		{
			generationProperties->illumination_distance = glm::clamp(generationProperties->illumination_distance + 0.01f, 0.01f, 10.0f);
		}
		else
		{
			generationProperties->illumination_distance = glm::clamp(generationProperties->illumination_distance - 0.01f, 0.01f, 10.0f);
		}

		context->setUniform1f("illumination_distance", generationProperties->lm == CAMERA ? generationProperties->illumination_distance * 10.0f : generationProperties->illumination_distance);
	}

	//TODO make this cycle through enumerated lighting modes, implement lighting modes
	if (inputTracker->isPressed(GLFW_KEY_8, false))
	{
		cycleEnum<lighting_mode>(lighting_mode(0), LIGHTING_MODE_SIZE, generationProperties->lm);
		if (generationProperties->lm == LIGHTING_MODE_SIZE)
		{
			generationProperties->lm = lighting_mode(0);
		}

		cout << "lighting mode: " << getStringFromLightingMode(generationProperties->lm) << endl;

		context->setUniform1i("lighting_mode", int(generationProperties->lm));
		context->setUniform1i("illumination_distance", generationProperties->lm == CAMERA ? generationProperties->illumination_distance * 10.0f : generationProperties->illumination_distance);
	}
}

void Renderer::adjustLineWidth(boolean increase)
{
	GLfloat width_range[2];
	glGetFloatv(GL_ALIASED_LINE_WIDTH_RANGE, width_range);

	float increment = increase ? 1.0f : -1.0f;
	generationProperties->line_width = glm::clamp(generationProperties->line_width + increment, width_range[0], width_range[1]);
	glLineWidth(GLfloat(generationProperties->line_width));
}
void Renderer::updateBackground(const shared_ptr<ArtForm>& artForm) const
{
	vec4 actual_background;
	if (generationProperties->no_background)
	{
		actual_background = generationProperties->inverted ? vec4(1.0f, 1.0f, 1.0f, 1.0f) : vec4(0.0f, 0.0f, 0.0f, 1.0f);
	}
	else
	{
		vec4 new_background = influenceElement<vec4>(artForm->getColorsBack().at(generationProperties->background_back_index), artForm->getColorsFront().at(generationProperties->background_front_index), generationProperties->interpolation_state);
		colorManager->adjustLightness(new_background, 0.1f);

		if (generationProperties->inverted)
		{
			new_background = vec4(1.0f) - new_background;
		}

		actual_background = new_background;
	}

	context->setBackgroundColor(actual_background);
	context->setUniform4fv("background_color", 1, actual_background);
}

void Renderer::invertColors(const shared_ptr<ArtForm>& artForm)
{
	generationProperties->inverted = !generationProperties->inverted;
	context->setUniform1i("invert_colors", generationProperties->inverted ? 1 : 0);
	updateBackground(artForm);
}

void Renderer::updateLightColorOverride(const shared_ptr<ArtForm>& artForm)
{
	context->setUniform1i("override_light_color_enabled", light_color_override_index != -3);

	vec4 light_color;
	switch (light_color_override_index)
	{
	case -3: break;
	case -2: light_color = vec4(0.0f, 0.0f, 0.0f, 1.0f); break;
	case -1: light_color = vec4(1.0f, 1.0f, 1.0f, 1.0f); break;
	default: light_color = influenceElement<vec4>(artForm->getColorsBack().at(light_color_override_index), artForm->getColorsFront().at(light_color_override_index), generationProperties->interpolation_state); break;
	}

	if (light_color_override_index != -3)
		context->setUniform4fv("light_override_color", 1, light_color);
}

void Renderer::updateLineColorOverride(const shared_ptr<ArtForm>& artForm)
{
	context->setUniform1i("override_line_color_enabled", line_color_override_index != -3);

	vec4 line_color;
	switch (line_color_override_index)
	{
	case -3: break;
	case -2: line_color = vec4(0.0f, 0.0f, 0.0f, 1.0f); break;
	case -1: line_color = vec4(1.0f, 1.0f, 1.0f, 1.0f); break;
	default: line_color = influenceElement<vec4>(artForm->getColorsBack().at(line_color_override_index), artForm->getColorsFront().at(line_color_override_index), generationProperties->interpolation_state); break;
	}

	if (line_color_override_index != -3)
	{
		context->setUniform4fv("line_override_color", 1, line_color);
	}
}

void Renderer::updateTriangleColorOverride(const shared_ptr<ArtForm>& artForm)
{
	context->setUniform1i("override_triangle_color_enabled", triangle_color_override_index != -3);

	vec4 triangle_color;
	switch (triangle_color_override_index)
	{
	case -3: break;
	case -2: triangle_color = vec4(0.0f, 0.0f, 0.0f, 1.0f); break;
	case -1: triangle_color = vec4(1.0f, 1.0f, 1.0f, 1.0f); break;
	default: triangle_color = influenceElement<vec4>(artForm->getColorsBack().at(triangle_color_override_index), artForm->getColorsFront().at(triangle_color_override_index), generationProperties->interpolation_state); break;
	}

	if (triangle_color_override_index != -3)
		context->setUniform4fv("triangle_override_color", 1, triangle_color);
}

void Renderer::updatePointColorOverride(const shared_ptr<ArtForm>& artForm)
{
	context->setUniform1i("override_point_color_enabled", point_color_override_index != -3);

	vec4 point_color;
	switch (point_color_override_index)
	{
	case -3: break;
	case -2: point_color = vec4(0.0f, 0.0f, 0.0f, 1.0f); break;
	case -1: point_color = vec4(1.0f, 1.0f, 1.0f, 1.0f); break;
	default: point_color = influenceElement<vec4>(artForm->getColorsBack().at(point_color_override_index), artForm->getColorsFront().at(point_color_override_index), generationProperties->interpolation_state); break;
	}

	if (point_color_override_index != -3)
	{
		context->setUniform4fv("point_override_color", 1, point_color);
	}
}

void Renderer::applyBackground(const int &num_samples)
{
	vector<vec4> colorsFront = artForm->getColorsFront();
	background_color = generationProperties->inverted ? vec4(1.0f) - getSampleColor(num_samples, colorsFront) : getSampleColor(num_samples, colorsFront);

	colorManager->adjustLightness(background_color, jep::floatRoll(0.0f, 1.0f, 2));
	context->setBackgroundColor(background_color);
}

void Renderer::adjustBackgroundBrightness(float adjustment)
{
	float current_lightness = colorManager->getHSLFromRGBA(background_color).L;
	colorManager->adjustLightness(background_color, current_lightness + adjustment);
	context->setBackgroundColor(background_color);
}

void Renderer::cycleBackgroundColorIndex(const shared_ptr<ArtForm>& artForm)
{
	generationProperties->background_front_index + 1 == artForm->getColorsFront().size() ? generationProperties->background_front_index = 0 : generationProperties->background_front_index++;
	generationProperties->background_back_index = generationProperties->background_front_index;
	updateBackground(artForm);
}

//TODO consolidate below
void Renderer::cycleLightColorOverride(const shared_ptr<ArtForm>& artForm)
{
	if (light_color_override_index == artForm->getColorsFront().size() - 1)
	{
		light_color_override_index = -3;
	}
	else
	{
		light_color_override_index++;
	}

	cout << "light color override index: " << light_color_override_index << endl;

	updateLightColorOverride(artForm);
}

void Renderer::cycleLineColorOverride(const shared_ptr<ArtForm>& artForm)
{
	if (line_color_override_index == artForm->getColorsFront().size() - 1)
	{
		line_color_override_index = -3;
	}
	else
	{
		line_color_override_index++;
	}

	cout << "line color override index: " << line_color_override_index << endl;

	updateLineColorOverride(artForm);
}

void Renderer::cycleTriangleColorOverride(const shared_ptr<ArtForm>& artForm)
{
	if (triangle_color_override_index == artForm->getColorsFront().size() - 1)
	{
		triangle_color_override_index = -3;
	}
	else
	{
		triangle_color_override_index++;
	}

	cout << "triangle color override index: " << triangle_color_override_index << endl;

	updateTriangleColorOverride(artForm);
}

void Renderer::cyclePointColorOverride(const shared_ptr<ArtForm>& artForm)
{
	if (point_color_override_index == artForm->getColorsFront().size() - 1)
	{
		point_color_override_index = -3;
	}
	else
	{
		point_color_override_index++;
	}

	cout << "point color override index: " << point_color_override_index << endl;

	updatePointColorOverride(artForm);
}

vec4 Renderer::getSampleColor(const int &samples, const vector<vec4> &color_pool) const
{
	if (samples > color_pool.size())
	{
		cout << "color samples requested are greater than the number of colors in the targeted generator" << endl;
		return vec4(0.0f, 0.0f, 0.0f, 1.0f);
	}

	vec4 out_color(0.0f, 0.0f, 0.0f, 0.0f);

	if (samples <= 0)
	{
		return out_color;
	}

	for (int i = 0; i < samples; i++)
	{
		int random_index = (int)(randomGenerator->getRandomUniform() * color_pool.size());
		out_color += color_pool.at(random_index);
	}

	out_color /= (float)samples;
	return out_color;
}