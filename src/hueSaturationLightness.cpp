#include "hueSaturationLightness.h"

bool hueSaturationLightness::operator==(const hueSaturationLightness& other)
{
	return (H == other.H) && (S == other.S) && (L == other.L);
}