#include "ArtForm.h"

ArtForm::ArtForm(
	const shared_ptr<ogl_context>& _context,
	const shared_ptr<ColorManager>& _colorManager,
	const shared_ptr<Bufferer>& _bufferer,
	const shared_ptr<RandomGenerator>& _randomGenerator,
	const shared_ptr<GenerationProperties>& _generationProperties)
{
	colorManager = _colorManager;
	bufferer = _bufferer;

	context = context;
	randomGenerator = _randomGenerator;
	generationProperties = _generationProperties;
	vertex_count = generationProperties->num_points;
	generateLights();
	setMatrices();

	vertex_size = bufferer->getVertexSize();
}

vector< pair<string, mat4> > ArtForm::generateMatrixVector(const int &count, geometry_type &geo_type)
{
	vector< pair<string, mat4> > matrix_vector;

	if (randomGenerator->getRandomFloat() < generationProperties->matrix_geometry_coefficient)
	{
		vector<vec4> point_sequence;
		int matrix_geometry_index;
		// TODO create mc exception class
		/*if (loaded_sequences.size() > 0)
			sm.matrix_geometry_weights[LOADED_SEQUENCE] = mc.getRandomIntInRange(0, loaded_sequences.size() * 10);*/

		if (!randomGenerator->catRoll<int>(generationProperties->matrix_geometry_weights, matrix_geometry_index))
		{
			throw;
		}

		float random_width = randomGenerator->getRandomFloatInRange(0.2f, 1.0f);
		float random_height = randomGenerator->getRandomFloatInRange(0.2f, 1.0f);
		float random_depth = randomGenerator->getRandomFloatInRange(0.2f, 1.0f);

		if (matrix_geometry_index < GEOMETRY_TYPE_SIZE)
		{
			geometry_type gt = geometry_type(matrix_geometry_index);

			float random_width = randomGenerator->getRandomFloatInRange(0.2f, 1.0f);
			float random_height = randomGenerator->getRandomFloatInRange(0.2f, 1.0f);
			float random_depth = randomGenerator->getRandomFloatInRange(0.2f, 1.0f);

			switch (gt)
			{
			case CUBOID: point_sequence = gm.getCuboidVertices(random_width, random_height, random_depth); break;
			case CUBE: point_sequence = gm.getCubeVertices(random_width); break;
			case TETRAHEDRON: point_sequence = gm.getTetrahedronVertices(random_width); break;
			case OCTAHEDRON: point_sequence = gm.getOctahedronVertices(random_width); break;
			case DODECAHEDRON: point_sequence = gm.getDodecahedronVertices(random_width); break;
			case ICOSAHEDRON: point_sequence = gm.getIcosahedronVertices(random_width); break;
				//case LOADED_SEQUENCE: geo_type = DEFAULT_GEOMETRY_TYPE;
			case GEOMETRY_TYPE_SIZE:
			default: throw;
			}
		}
		else
		{
			matrix_geometry_index -= (int)GEOMETRY_TYPE_SIZE;
			ngon_type nt = ngon_type(matrix_geometry_index);
			int side_count = (int)nt + 3;
			point_sequence = gm.getNgonVertices(randomGenerator->getRandomFloatInRange(0.2f, 1.0f), side_count);
		}

		for (int i = 0; i < count; i++)
		{
			vec3 vertex(point_sequence.at(i % point_sequence.size()));
			matrix_vector.push_back(std::pair<string, mat4>("translate (" + std::to_string(matrix_geometry_index) + ")", glm::translate(mat4(1.0f), vertex)));
		}
	}

	else
	{
		geo_type = GEOMETRY_TYPE_SIZE;
		int matrix_type;

		for (int i = 0; i < count; i++)
		{
			short matrix_type;
			std::map<short, unsigned int> matrix_map;
			matrix_map[0] = generationProperties->translate_weight;
			matrix_map[1] = generationProperties->rotate_weight;
			matrix_map[2] = generationProperties->scale_matrices ? generationProperties->scale_weight : 0;

			// TODO create mc exception class
			if (!randomGenerator->catRoll<short>(matrix_map, matrix_type))
				throw;

			string matrix_category;
			mat4 matrix_to_add;

			switch (matrix_type)
			{
			case 0:
				matrix_to_add = generationProperties->two_dimensional ? randomGenerator->getRandomTranslation2D() : randomGenerator->getRandomTranslation();
				matrix_category = "translate";
				break;
			case 1:
				matrix_to_add = generationProperties->two_dimensional ? randomGenerator->getRandomRotation2D() : randomGenerator->getRandomRotation();
				matrix_category = "rotate";
				break;
			case 2:
				matrix_to_add = generationProperties->two_dimensional ? randomGenerator->getRandomScale2D() : randomGenerator->getRandomScale();
				matrix_category = "scale";
				break;
			default: break;
			}

			matrix_vector.push_back(pair<string, mat4>(matrix_category, matrix_to_add));
		}
	}

	return matrix_vector;
}

vector<vec4> ArtForm::generateColorVector(const vec4 &seed, color_palette palette, const int &count, color_palette &random_selection) const
{
	vector<vec4> color_set;

	color_set = colorManager->generatePaletteFromSeed(seed, palette, count, random_selection);

	if (generationProperties->randomize_alpha)
	{
		colorManager->randomizeAlpha(color_set, generationProperties->alpha_min, generationProperties->alpha_max);
	}

	if (generationProperties->randomize_lightness)
	{
		colorManager->modifyLightness(color_set, randomGenerator->getRandomFloatInRange(0.3, 1.2f));
	}

	return color_set;
}

vector<float> ArtForm::generateSizeVector(const int &count) const
{
	vector<float> size_vector;

	for (int i = 0; i < count; i++)
	{
		size_vector.push_back(randomGenerator->getRandomFloatInRange(POINT_SCALE_MIN, POINT_SCALE_MAX));
	}

	return size_vector;
}

// this method is run once and only once per fractal gen object
void ArtForm::setMatrices()
{
	randomGenerator->seed(getGenerationSeed());

	for (int i = 0; i < vertex_count; i++)
	{
		matrix_sequence_front.push_back(int(randomGenerator->getRandomFloatInRange(0.0f, float(generationProperties->num_matrices))));
		matrix_sequence_back.push_back(int(randomGenerator->getRandomFloatInRange(0.0f, float(generationProperties->num_matrices))));
	}

	int random_palette_index = int(randomGenerator->getRandomFloatInRange(0.0f, float(DEFAULT_COLOR_PALETTE)));
	generationProperties->palette_front = color_palette(random_palette_index);
	generationProperties->palette_back = color_palette(random_palette_index);

	//TODO add .reserve() for each vector
	matrices_front.clear();
	colors_front.clear();
	sizes_front.clear();

	matrices_back.clear();
	colors_back.clear();
	sizes_back.clear();

	//front data set
	matrices_front = generateMatrixVector(generationProperties->num_matrices, geo_type_front);
	seed_color_front = randomGenerator->getRandomVec4FromColorRanges(
		0.0f, 1.0f,		// red range
		0.0f, 1.0f,		// green range
		0.0f, 1.0f,		// blue range
		generationProperties->alpha_min, generationProperties->alpha_max		// alpha range
		);
	colors_front = generateColorVector(seed_color_front, generationProperties->palette_front, generationProperties->num_matrices, generationProperties->random_palette_front);
	sizes_front = generateSizeVector(generationProperties->num_matrices);
	
	//back data set
	generation++;
	randomGenerator->seed(getGenerationSeed());
	matrices_back = generateMatrixVector(generationProperties->num_matrices, geo_type_back);
	//std::random_shuffle(matrices_front.begin(), matrices_front.end());
	seed_color_back = randomGenerator->getRandomVec4FromColorRanges(
		0.0f, 1.0f,		// red range
		0.0f, 1.0f,		// green range
		0.0f, 1.0f,		// blue range
		generationProperties->alpha_min, generationProperties->alpha_max		// alpha range
		);
	colors_back = generateColorVector(seed_color_back, generationProperties->palette_back, generationProperties->num_matrices, generationProperties->random_palette_back);
	sizes_back = generateSizeVector(generationProperties->num_matrices);
}

void ArtForm::swapMatrices() 
{
	if (generationProperties->reverse)
	{
		generation--;
	}
	else
	{
		generation++;
	}

	randomGenerator->seed(getGenerationSeed());

	//TODO use pointers instead of loaded if statements
	if (!generationProperties->reverse)
	{
		matrices_back = matrices_front;
		vector<unsigned int> matrix_sequence_temp(matrix_sequence_back);
		matrix_sequence_back = matrix_sequence_front;
		matrix_sequence_front = matrix_sequence_temp;
		colors_back = colors_front;
		sizes_back = sizes_front;
		seed_color_back = seed_color_front;
		generationProperties->background_back_index = generationProperties->background_front_index;
		generationProperties->background_front_index = randomGenerator->getRandomIntInRange(0, colors_front.size());

		seed_color_front = randomGenerator->getRandomVec4FromColorRanges(
			0.0f, 1.0f,		// red range
			0.0f, 1.0f,		// green range
			0.0f, 1.0f,		// blue range
			generationProperties->alpha_min, generationProperties->alpha_max		// alpha range
			);

		matrices_front = generateMatrixVector(matrices_back.size(), geo_type_front);
		colors_front = generateColorVector(seed_color_front, generationProperties->palette_front, matrices_back.size(), generationProperties->random_palette_front);
		sizes_front = generateSizeVector(matrices_back.size());
	}

	else
	{
		matrices_front = matrices_back;
		vector<unsigned int> matrix_sequence_temp(matrix_sequence_front);
		matrix_sequence_front = matrix_sequence_back;
		matrix_sequence_back = matrix_sequence_temp;
		colors_front = colors_back;
		sizes_front = sizes_back;
		seed_color_front = seed_color_back;
		generationProperties->background_front_index = generationProperties->background_back_index;
		generationProperties->background_back_index = randomGenerator->getRandomIntInRange(0, colors_back.size());

		seed_color_back = randomGenerator->getRandomVec4FromColorRanges(
			0.0f, 1.0f,		// red range
			0.0f, 1.0f,		// green range
			0.0f, 1.0f,		// blue range
			generationProperties->alpha_min, generationProperties->alpha_max		// alpha range
			);

		matrices_back = generateMatrixVector(matrices_front.size(), geo_type_back);
		colors_back = generateColorVector(seed_color_back, generationProperties->palette_back, matrices_front.size(), generationProperties->random_palette_back);
		sizes_back = generateSizeVector(matrices_front.size());
	}

	if (generationProperties->print_context_on_swap)
	{
		printContext();
	}
}

string ArtForm::getGenerationSeed()
{
	return getBaseSeed() + "_" + std::to_string(generation);
}

void ArtForm::printMatrices() const
{
	cout << "-----matrices_front-----" << endl;
	for (const auto &matrix_pair : matrices_front)
	{
		cout << matrix_pair.first << endl;
		cout << glm::to_string(matrix_pair.second) << endl;
		cout << "----------" << endl;
	}

	cout << "-----matrices_back-----" << endl;
	for (const auto &matrix_pair : matrices_back)
	{
		cout << matrix_pair.first << endl;
		cout << glm::to_string(matrix_pair.second) << endl;
		cout << "----------" << endl;
	}

	cout << "------------------" << endl;
}

void ArtForm::generateFractalFromPointSequence()
{
	vector<float> points;
	vector<unsigned short> line_indices_to_buffer;
	vector<unsigned short> triangle_indices_to_buffer;
	points.reserve(vertex_count * vertex_size);

	int num_matrices = matrices_front.size();

	vec4 point_color = generationProperties->inverted ? vec4(0.0f, 0.0f, 0.0f, 1.0f) : vec4(1.0f);
	float starting_size = POINT_SCALE_MAX;

	mat4 origin_matrix = glm::scale(mat4(1.0f), vec3(1.0f, 1.0f, 1.0f));

	int current_sequence_index_lines = 0;
	int current_sequence_index_triangles = 0;

	for (int i = 0; i < vertex_count / generationProperties->point_sequence.size(); i++)
	{
		int matrix_index_front = generationProperties->smooth_render ? matrix_sequence_front.at(i) : int(randomGenerator->getRandomFloatInRange(0.0f, float(matrices_front.size())));
		int matrix_index_back = generationProperties->smooth_render ? matrix_sequence_back.at(i) : int(randomGenerator->getRandomFloatInRange(0.0f, float(matrices_front.size())));
		vec4 transformation_color = influenceElement<vec4>(colors_back.at(matrix_index_back), colors_front.at(matrix_index_front), generationProperties->interpolation_state);
		float transformation_size = influenceElement<float>(sizes_back.at(matrix_index_back), sizes_front.at(matrix_index_front), generationProperties->interpolation_state);

		addPointSequenceAndIterate(origin_matrix, point_color, starting_size, matrix_index_front, matrix_index_back, points, line_indices_to_buffer, triangle_indices_to_buffer, current_sequence_index_lines, current_sequence_index_triangles);
	}

	updateIndexCountsAndBufferData(line_indices_to_buffer, triangle_indices_to_buffer, points);
}

void ArtForm::generateFractal()
{
	vector<float> points;
	vector<unsigned short> line_indices_to_buffer;
	vector<unsigned short> triangle_indices_to_buffer;
	points.reserve(vertex_count * vertex_size);

	int num_matrices = matrices_front.size();

	vec4 starting_point = origin;
	vec4 point_color = generationProperties->inverted ? vec4(0.0f, 0.0f, 0.0f, 1.0f) : vec4(1.0f);
	float starting_size = POINT_SCALE_MAX;

	for (int i = 0; i < vertex_count && num_matrices > 0; i++)
	{
		int matrix_index_front = generationProperties->smooth_render ? matrix_sequence_front.at(i) : int(randomGenerator->getRandomFloatInRange(0.0f, float(matrices_front.size())));
		int matrix_index_back = generationProperties->smooth_render ? matrix_sequence_back.at(i) : int(randomGenerator->getRandomFloatInRange(0.0f, float(matrices_front.size())));

		addNewPointAndIterate(starting_point, point_color, starting_size, matrix_index_front, matrix_index_back, points);
		line_indices_to_buffer.push_back(line_indices_to_buffer.size());
		triangle_indices_to_buffer.push_back(triangle_indices_to_buffer.size());
	}

	updateIndexCountsAndBufferData(line_indices_to_buffer, triangle_indices_to_buffer, points);
}

void ArtForm::generateFractalWithRefresh()
{
	vector<float> points;
	vector<unsigned short> line_indices_to_buffer;
	vector<unsigned short> triangle_indices_to_buffer;
	points.reserve(vertex_count * vertex_size);

	int num_matrices = matrices_front.size();
	signed int actual_refresh = generationProperties->refresh_value == -1 ? int(randomGenerator->getRandomFloatInRange(generationProperties->refresh_min, generationProperties->refresh_max)) : generationProperties->refresh_value;

	for (int i = 0; i < vertex_count && num_matrices > 0; i++)
	{
		vec4 point_color = generationProperties->inverted ? vec4(0.0f, 0.0f, 0.0f, 1.0f) : vec4(1.0f);
		vec4 new_point = origin;
		float new_size = POINT_SCALE_MAX;

		for (int n = 0; n < actual_refresh; n++)
		{
			int matrix_index_front = generationProperties->smooth_render ? matrix_sequence_front.at((i + n) % matrix_sequence_front.size()) : int(randomGenerator->getRandomFloatInRange(0.0f, float(matrices_front.size())));
			int matrix_index_back = generationProperties->smooth_render ? matrix_sequence_back.at((i + n) % matrix_sequence_back.size()) : int(randomGenerator->getRandomFloatInRange(0.0f, float(matrices_back.size())));

			mat4 matrix_front = matrices_front.at(matrix_index_front).second;
			mat4 matrix_back = matrices_back.at(matrix_index_back).second;
			vec4 point_front = matrix_front * new_point;
			vec4 point_back = matrix_back * new_point;

			generationProperties->interpolation_state = glm::clamp(generationProperties->interpolation_state, 0.0f, 1.0f);

			vec4 transformation_color = influenceElement<vec4>(colors_back.at(matrix_index_back), colors_front.at(matrix_index_front), generationProperties->interpolation_state);
			float transformation_size = influenceElement<float>(sizes_back.at(matrix_index_back), sizes_front.at(matrix_index_front), generationProperties->interpolation_state);
			new_point = influenceElement<vec4>(point_back, point_front, generationProperties->interpolation_state);

			point_color += transformation_color;
    			new_size += transformation_size;
		}

		point_color /= ((float)actual_refresh + 1.0f);
		new_size /= ((float)actual_refresh + 1.0f);

		addNewPoint(new_point, point_color, new_size, points);
		line_indices_to_buffer.push_back(line_indices_to_buffer.size());
		triangle_indices_to_buffer.push_back(triangle_indices_to_buffer.size());
	}

	updateIndexCountsAndBufferData(line_indices_to_buffer, triangle_indices_to_buffer, points);
}

void ArtForm::generateFractalFromPointSequenceWithRefresh()
{
	vector<float> points;
	vector<unsigned short> line_indices_to_buffer;
	vector<unsigned short> triangle_indices_to_buffer;
	points.reserve(vertex_count * vertex_size);

	int num_matrices = matrices_front.size();

	signed int actual_refresh = generationProperties->refresh_value == -1 ? int(randomGenerator->getRandomFloatInRange(generationProperties->refresh_min, generationProperties->refresh_max)) : generationProperties->refresh_value;

	int current_sequence_index_lines = 0;
	int current_sequence_index_triangles = 0;

	for (int i = 0; i < vertex_count / generationProperties->point_sequence.size() && num_matrices > 0; i++)
	{
		vec4 final_color = generationProperties->inverted ? vec4(0.0f, 0.0f, 0.0f, 1.0f) : vec4(1.0f);
		float final_size = POINT_SCALE_MAX;
		mat4 final_matrix = glm::scale(mat4(1.0f), vec3(1.0f, 1.0f, 1.0f));

		for (int n = 0; n < actual_refresh; n++)
		{
			int matrix_index_front = generationProperties->smooth_render ? matrix_sequence_front.at((i + n) % matrix_sequence_front.size()) : int(randomGenerator->getRandomFloatInRange(0.0f, float(matrices_front.size())));
			int matrix_index_back = generationProperties->smooth_render ? matrix_sequence_back.at((i + n) % matrix_sequence_back.size()) : int(randomGenerator->getRandomFloatInRange(0.0f, float(matrices_back.size())));

			mat4 matrix_front = matrices_front.at(matrix_index_front).second;
			mat4 matrix_back = matrices_back.at(matrix_index_back).second;
			mat4 interpolated_matrix = influenceElement<mat4>(matrix_back, matrix_front, generationProperties->interpolation_state);
			vec4 transformation_color = influenceElement<vec4>(colors_back.at(matrix_index_back), colors_front.at(matrix_index_front), generationProperties->interpolation_state);
			float transformation_size = influenceElement<float>(sizes_back.at(matrix_index_back), sizes_front.at(matrix_index_front), generationProperties->interpolation_state);

			final_matrix = interpolated_matrix * final_matrix;
			final_color += transformation_color;
			final_size += transformation_size;
		}

		final_color /= float(actual_refresh + 1);
		final_size /= float(actual_refresh + 1);

		int index_sequences_added = points.size() / (generationProperties->point_sequence.size() * vertex_size);

		// determine where values should begin based on the used sequence and number of indices already added

		vector<int>::iterator max_local_value_lines = std::max_element(generationProperties->line_indices.begin(), generationProperties->line_indices.end());
		int starting_index_lines = index_sequences_added * (*max_local_value_lines + 1);
		for (const unsigned short index : generationProperties->line_indices)
		{
			line_indices_to_buffer.push_back(starting_index_lines + index);
		}

		vector<int>::iterator max_local_value_triangles = std::max_element(generationProperties->triangle_indices.begin(), generationProperties->triangle_indices.end());
		int starting_index_triangles = index_sequences_added * (*max_local_value_triangles + 1);
		for (const unsigned short index : generationProperties->triangle_indices)
		{
			triangle_indices_to_buffer.push_back(starting_index_triangles + index);
		}

		for (int n = 0; n < generationProperties->point_sequence.size(); n++)
		{
			addNewPoint(final_matrix * generationProperties->point_sequence.at(n), final_color, final_size, points);
		}
	}

	updateIndexCountsAndBufferData(line_indices_to_buffer, triangle_indices_to_buffer, points);
}

void ArtForm::updateIndexCountsAndBufferData(std::vector<unsigned short> &line_indices_to_buffer, std::vector<unsigned short> &triangle_indices_to_buffer, std::vector<float> &points)
{
	line_index_count = line_indices_to_buffer.size();
	triangle_index_count = triangle_indices_to_buffer.size();

	vertex_count = points.size() / bufferer->getVertexSize();
	generationProperties->enable_triangles = vertex_count >= 3;
	generationProperties->enable_lines = vertex_count >= 2;

	bufferer->bufferLightData(points, light_indices);

	bufferer->bufferData(points, line_indices_to_buffer, triangle_indices_to_buffer);
}


void ArtForm::renderFractal(const int &image_width, const int &image_height, const int &matrix_sequence_count)
{
	char cFileName[64];
	FILE *fScreenshot;

	int nSize = image_width * image_height * 3;
	GLubyte *pixels = new GLubyte[nSize];
	if (pixels == NULL) return;

	int nShot = 0;

	while (nShot < 64)
	{
		sprintf_s(cFileName, "screenshot_%d.tga", nShot);
		errno_t error = fopen_s(&fScreenshot, cFileName, "rb");

		if (error != 0)
		{
			break;
		}
		else
		{
			fclose(fScreenshot);
		}

		++nShot;

		if (nShot > 63)
		{
			cout << "Screenshot limit of 64 reached. Remove some shots if you want to take more." << endl;
			return;
		}
	}

	errno_t error = fopen_s(&fScreenshot, cFileName, "wb");

	vector<mat4> matrix_sequence = generateMatrixSequence(10);
	map<int, int> calc_map;
	vec4 white(1.0f, 1.0f, 1.0f, 1.0f);
	mat4 scale_matrix = glm::scale(mat4(1.0f), vec3(1.0f, 1.0f, 1.0f));

	//convert to BGR format    
	unsigned char temp;
	for (int i = 0; i < nSize; i += 3)
	{
		unsigned int calc_counter = 0;
		int x_coord = (i/3) % image_width;
		int y_coord = (i/3) / image_width;
		float x_pos = ((float)x_coord / (float)image_width);
		float y_pos = ((float)y_coord / (float)image_height);
		vec4 uv_point((x_pos * 2.0f) - 1.0f, (y_pos * 2.0f) - 1.0f, 0.0f, 1.0f);

		float x = 0.0f;
		float y = 0.0f;

		while (uv_point.x < 1.1f && uv_point.x > -1.1f  && uv_point.y < 1.1f  && uv_point.y > -1.1f && calc_counter < 1000)
		{
			mat4 current_matrix = scale_matrix * matrix_sequence.at(calc_counter % matrix_sequence.size());
			uv_point = current_matrix * uv_point;

			calc_counter++;
		}

		if (calc_map.find(calc_counter) == calc_map.end())
			calc_map[calc_counter] = 1;

		else calc_map[calc_counter] += 1;

		//dependend on fractal seed, replace
		int color_index = calc_counter / 100;
		float color_value = (float)color_index / 10.0f;

		pixels[i] = GLubyte(color_value * 255.0f);
		pixels[i+1] = GLubyte(color_value * 255.0f);
		pixels[i+2] = GLubyte((1.0f - color_value) * 255.0f);

		calc_counter = 0;
	}

	for (const auto &calc_pair : calc_map)
	{
		cout << calc_pair.first << " calcs: " << calc_pair.second << endl;
	}

	unsigned char TGAheader[12] = { 0,0,2,0,0,0,0,0,0,0,0,0 };
	unsigned char header[6] = { image_width % 256,image_width / 256, image_height % 256,image_height / 256,24,0 };

	fwrite(TGAheader, sizeof(unsigned char), 12, fScreenshot);
	fwrite(header, sizeof(unsigned char), 6, fScreenshot);
	fwrite(pixels, sizeof(GLubyte), nSize, fScreenshot);
	fclose(fScreenshot);

	delete[] pixels;

	return;
}

vector<mat4> ArtForm::generateMatrixSequence(const int &sequence_size) const
{
	vector<mat4> matrix_sequence;

	for (int i = 0; i < sequence_size; i++)
	{
		int random_index = randomGenerator->getRandomUniform() * (float)matrices_front.size();
		matrix_sequence.push_back(matrices_front.at(random_index).second);
	}

	//return matrix_sequence;

	vector<mat4> dummy_sequence = {
		glm::translate(mat4(1.0f), vec3(0.01f, 0.01f, 0.0f)),
		glm::rotate(mat4(1.0f), 0.05f, vec3(0.0f, 0.0f, 1.0f)),
		glm::translate(mat4(1.0f), vec3(0.02f, -0.01f, 0.0f)),
		glm::translate(mat4(1.0f), vec3(-0.01f, 0.02f, 0.0f)),
		glm::rotate(mat4(1.0f), 0.02f, vec3(0.0f, 0.0f, 1.0f))
	};

	return dummy_sequence;
}

void ArtForm::addNewPointAndIterate(
	vec4 &starting_point,
	vec4 &starting_color,
	float &starting_size,
	int matrix_index_front,
	int matrix_index_back,
	vector<float> &points)
{
	mat4 matrix_front = matrices_front.at(matrix_index_front).second;
	mat4 matrix_back = matrices_back.at(matrix_index_back).second;
	vec4 point_front = matrix_front * starting_point;
	vec4 point_back = matrix_back * starting_point;

	vec4 matrix_color_front = influenceElement<vec4>(starting_color, colors_front.at(matrix_index_front), generationProperties->bias_coefficient);
	vec4 matrix_color_back = influenceElement<vec4>(starting_color, colors_back.at(matrix_index_back), generationProperties->bias_coefficient);

	float point_size_front = influenceElement<float>(starting_size, sizes_front.at(matrix_index_front), generationProperties->bias_coefficient);
	float point_size_back = influenceElement<float>(starting_size, sizes_back.at(matrix_index_back), generationProperties->bias_coefficient);

	starting_point = influenceElement<vec4>(point_back, point_front, generationProperties->interpolation_state);
	starting_color = influenceElement<vec4>(matrix_color_back, matrix_color_front, generationProperties->interpolation_state);
	starting_size = influenceElement<float>(point_size_back, point_size_front, generationProperties->interpolation_state);

	vec4 point_to_add = starting_point;

	if (points.size() == 0)
	{
		centroid = vec3(0.0f);
		average_delta = 0.0f;
		max_x = 0.0f;
		max_y = 0.0f;
		max_z = 0.0f;
	}

	float current_point_count = points.size();

	centroid = ((current_point_count * centroid) + vec3(point_to_add)) / (current_point_count + 1.0f);
	float delta = glm::length(vec3(point_to_add) - centroid);
	average_delta = ((current_point_count * average_delta) + delta) / (current_point_count + 1.0f);

	if (point_to_add.x > max_x)
		max_x = point_to_add.x;

	if (point_to_add.y > max_y)
		max_y = point_to_add.y;

	if (point_to_add.z > max_z)
		max_z = point_to_add.z;

	points.push_back((float)(point_to_add.x));
	points.push_back((float)(point_to_add.y));
	points.push_back((float)(point_to_add.z));
	points.push_back((float)(starting_point.w));

	points.push_back((float)(starting_color.r));
	points.push_back((float)(starting_color.g));
	points.push_back((float)(starting_color.b));
	points.push_back((float)(starting_color.a));
	points.push_back(starting_size);
}

void ArtForm::addPointSequenceAndIterate(
	mat4 &origin_matrix,
	vec4 &starting_color,
	float &starting_size,
	int matrix_index_front,
	int matrix_index_back,
	vector<float> &points,
	vector<unsigned short> &line_indices,
	vector<unsigned short> &triangle_indices,
	int &current_sequence_index_lines,
	int &current_sequence_index_triangles)
{
	mat4 matrix_front = matrices_front.at(matrix_index_front).second;
	mat4 matrix_back = matrices_back.at(matrix_index_back).second;
	mat4 interpolated_matrix = influenceElement<mat4>(matrix_back, matrix_front, generationProperties->interpolation_state);
	mat4 final_matrix = interpolated_matrix * origin_matrix;

	vec4 matrix_color_front = influenceElement<vec4>(starting_color, colors_front.at(matrix_index_front), generationProperties->bias_coefficient);
	vec4 matrix_color_back = influenceElement<vec4>(starting_color, colors_back.at(matrix_index_back), generationProperties->bias_coefficient);

	float point_size_front = influenceElement<float>(starting_size, sizes_front.at(matrix_index_front), generationProperties->bias_coefficient);
	float point_size_back = influenceElement<float>(starting_size, sizes_back.at(matrix_index_back), generationProperties->bias_coefficient);

	starting_color = influenceElement<vec4>(matrix_color_back, matrix_color_front, generationProperties->interpolation_state);
	starting_size = influenceElement<float>(point_size_back, point_size_front, generationProperties->interpolation_state);

	for (const vec4 &point : generationProperties->point_sequence)
	{
		vec4 point_to_add = final_matrix * point;

		if (points.size() == 0)
		{
			centroid = vec3(0.0f);
			average_delta = 0.0f;
			max_x = 0.0f;
			max_y = 0.0f;
			max_z = 0.0f;
		}

		float current_point_count = points.size();

		centroid = ((current_point_count * centroid) + vec3(point_to_add)) / (current_point_count + 1.0f);
		float delta = glm::length(vec3(point_to_add) - centroid);
		average_delta = ((current_point_count * average_delta) + delta) / (current_point_count + 1.0f);

		if (point_to_add.x > max_x)
			max_x = point_to_add.x;

		if (point_to_add.y > max_y)
			max_y = point_to_add.y;

		if (point_to_add.z > max_z)
			max_z = point_to_add.z;

		points.push_back((float)(point_to_add.x));
		points.push_back((float)(point_to_add.y));
		points.push_back((float)(point_to_add.z));
		points.push_back((float)(1.0f));

		points.push_back((float)(starting_color.r));
		points.push_back((float)(starting_color.g));
		points.push_back((float)(starting_color.b));
		points.push_back((float)(starting_color.a));
		points.push_back(starting_size);
	}	

	int local_points_size = generationProperties->point_sequence.size();
	int index_sequences_added = points.size() / (generationProperties->point_sequence.size() * vertex_size);

	// determine where values should begin based on the used sequence and number of indices already added
	vector<int>::iterator max_local_value_lines = std::max_element(generationProperties->line_indices.begin(), generationProperties->line_indices.end());
	int starting_index_lines = current_sequence_index_lines;
	for (const unsigned short index : generationProperties->line_indices)
	{
		line_indices.push_back(starting_index_lines + index);
	}

	current_sequence_index_lines += *max_local_value_lines;

	vector<int>::iterator max_local_value_triangles = std::max_element(generationProperties->triangle_indices.begin(), generationProperties->triangle_indices.end());
	int starting_index_triangles = current_sequence_index_triangles;
	for (const unsigned short index : generationProperties->triangle_indices)
	{
		triangle_indices.push_back(starting_index_triangles + index);
	}

	current_sequence_index_triangles += *max_local_value_triangles;

	origin_matrix = final_matrix;
}

void ArtForm::addNewPoint(
	const vec4 &point,
	const vec4 &color,
	const float &size,
	vector<float> &points)
{
	vec4 point_to_add = point;

	if (points.size() == 0)
	{
		centroid = vec3(0.0f);
		average_delta = 0.0f;
		max_x = 0.0f;
		max_y = 0.0f;
		max_z = 0.0f;
	}

	float current_point_count = points.size();

	centroid = ((current_point_count * centroid) + vec3(point_to_add)) / (current_point_count + 1.0f);
	float delta = glm::length(vec3(point_to_add) - centroid);
	average_delta = ((current_point_count * average_delta) + delta) / (current_point_count + 1.0f);

	if (point_to_add.x > max_x)
		max_x = point_to_add.x;

	if (point_to_add.y > max_y)
		max_y = point_to_add.y;

	if (point_to_add.z > max_z)
		max_z = point_to_add.z;

	points.push_back((float)point_to_add.x);
	points.push_back((float)point_to_add.y);
	points.push_back((float)point_to_add.z);
	points.push_back((float)point.w);

	points.push_back((float)color.r);
	points.push_back((float)color.g);
	points.push_back((float)color.b);
	points.push_back((float)color.a);
	points.push_back(size);
}

void ArtForm::update(const shared_ptr<InputTracker> &inputTracker)
{
	if (inputTracker->isPressed(GLFW_KEY_O, false))
	{
		//available
	}

	if (inputTracker->isPressed(GLFW_KEY_J, false))
	{
		show_growth = !show_growth;
	}

	if (inputTracker->isPressed(GLFW_KEY_G, false))
	{
		frame_increment = glm::clamp(int(frame_increment) * 2, 1, 100);
	}

	if (inputTracker->isPressed(GLFW_KEY_F, false))
	{
		frame_increment = glm::clamp(int(frame_increment) / 2, 1, 100);
	}

	if (inputTracker->isPressed(GLFW_KEY_H, false))
	{
		reverse_growth = !reverse_growth;
	}

	if (inputTracker->isPressed(GLFW_KEY_N, false))
	{
		regenerateFractal();
	}

	if (inputTracker->isPressed(GLFW_KEY_SEMICOLON, false))
	{
		printContext();
	}

	if (inputTracker->isPressed(GLFW_KEY_APOSTROPHE, false))
	{
		generationProperties->reverse = !generationProperties->reverse;
	}

	if (inputTracker->isPressed(GLFW_KEY_BACKSLASH, false))
	{
		generationProperties->print_context_on_swap = !generationProperties->print_context_on_swap;
	}

	if ((inputTracker->isPressed(GLFW_KEY_RIGHT_BRACKET, true) || inputTracker->isPressed(GLFW_KEY_LEFT_BRACKET, true)) && !inputTracker->shiftIsHeld()) 
	{
		float increment_min = .01f;
		float increment_max = .5f;
		float step_amount = .002f;

		if (inputTracker->isPressed(GLFW_KEY_RIGHT_BRACKET, true))
		{
			generationProperties->interpolation_increment += step_amount;
		}
		else
		{
			generationProperties->interpolation_increment -= step_amount;
		}

		generationProperties->interpolation_increment = glm::clamp(generationProperties->interpolation_increment, increment_min, increment_max);
		cout << "transition speed: " << generationProperties->interpolation_increment / increment_max << endl;
	}

	if (generationProperties->refresh_value != -1 && inputTracker->isPressed(GLFW_KEY_6, false))
	{
		generationProperties->refresh_value == generationProperties->refresh_min ? generationProperties->refresh_value = -1 : generationProperties->refresh_value--;
		cout << "refresh value: " << generationProperties->refresh_value << endl;
	}

	if (generationProperties->refresh_value != generationProperties->refresh_max && inputTracker->isPressed(GLFW_KEY_7, false))
	{
		generationProperties->refresh_value == -1 ? generationProperties->refresh_value = generationProperties->refresh_min : generationProperties->refresh_value++;
		cout << "refresh value: " << generationProperties->refresh_value << endl;
	}

	if (inputTracker->isPressed(GLFW_KEY_MINUS, false))
	{
		generationProperties->scale_matrices = !generationProperties->scale_matrices;
		generationProperties->scale_matrices ? cout << "scale matrices enabled" << endl : cout << "scale matrices disabled" << endl;
	}

	if (inputTracker->isPressed(GLFW_KEY_EQUAL, false))
	{
		generationProperties->refresh_enabled = !generationProperties->refresh_enabled;
	}

	if (inputTracker->isPressed(GLFW_KEY_3, false))
	{
		cycleGeometryType();
	}

	if (inputTracker->isPressed(GLFW_KEY_Y, false))
	{
		cycleColorPalette();
		cout << "front palette: " + colorManager->getPaletteName(generationProperties->palette_front) << endl;
		cout << "back palette: " + colorManager->getPaletteName(generationProperties->palette_back) << endl;
	}
}

void ArtForm::tickAnimation() 
{
	float increment_coefficient = 1.0f - (abs(0.5f - generationProperties->interpolation_state) * 2.0f);

	float actual_increment = glm::clamp(generationProperties->interpolation_increment * increment_coefficient * increment_coefficient, generationProperties->interpolation_increment * 0.05f, generationProperties->interpolation_increment);

	generationProperties->reverse ? generationProperties->interpolation_state -= actual_increment : generationProperties->interpolation_state += actual_increment;

	if (generationProperties->interpolation_state <= 0.0f)
	{
		generationProperties->interpolation_state = 1.0f;
		swapMatrices();
	}
	else if (generationProperties->interpolation_state >= 1.0f)
	{
		generationProperties->interpolation_state = 0.0f;
		swapMatrices();
	}

	regenerateFractal();

	if (show_growth)
	{
		if (reverse_growth)
		{
			current_frame <= frame_increment ? current_frame = 0 : current_frame -= frame_increment;
		}
		else
		{
			current_frame = glm::clamp(int(current_frame + frame_increment), 0, INT_MAX);
		}

		vertices_to_render = current_frame;
	}
}

void ArtForm::regenerateFractal()
{
	if (generationProperties->refresh_enabled)
	{
		if (generationProperties->use_point_sequence)
			generateFractalFromPointSequenceWithRefresh();

		else generateFractalWithRefresh();
	}

	else
	{
		if (generationProperties->use_point_sequence)
			generateFractalFromPointSequence();

		else generateFractal();
	}
}

void ArtForm::loadPointSequence(string name, const vector<vec4> &sequence)
{
	if (sequence.size() == 0)
		return;

	loaded_sequences.push_back(pair<string, vector<vec4> >(name, sequence));
}

void ArtForm::printContext()
{
	cout << "------------------------------------------------" << endl;
	cout << "base seed: " << getBaseSeed() << endl;
	cout << "generation seed: " << getGenerationSeed() << endl;
	cout << "current generation: " << generation;
	generationProperties->reverse ? cout << " <-" << endl : cout << " ->" << endl;

	printMatrices();

	cout << "point count: " << vertex_count << endl;
	generationProperties->refresh_enabled ? cout << "refresh enabled (" << generationProperties->refresh_value << ")" << endl : cout << "refresh disabled" << endl;
	cout << "center: " + glm::to_string(centroid) << endl;
	cout << "max x: " << max_x << endl;
	cout << "max y: " << max_y << endl;
	cout << "max z: " << max_z << endl;

	cout << "front palette: " + colorManager->getPaletteName(generationProperties->palette_front) << endl;
	if (generationProperties->palette_front == RANDOM_PALETTE)
		cout << "current front palette: " + colorManager->getPaletteName(generationProperties->random_palette_front) << endl;

	cout << "front color set, seed = " + colorManager->toRGBAString(seed_color_front) + ":" << endl;
	colorManager->printColorSet(colors_front);
	cout << "front background index: " << generationProperties->background_front_index << endl;
	cout << endl;

	cout << "back palette: " + colorManager->getPaletteName(generationProperties->palette_back) << endl;
	if (generationProperties->palette_back == RANDOM_PALETTE)
		cout << "current back palette: " + colorManager->getPaletteName(generationProperties->random_palette_back) << endl;

	cout << "back color set, seed = " + colorManager->toRGBAString(seed_color_back) + ":" << endl;
	colorManager->printColorSet(colors_back);
	cout << "back background index: " << generationProperties->background_back_index << endl;
	cout << endl;

	cout << "line width: " << generationProperties->line_width << endl;
	cout << "interpolation state: " << generationProperties->interpolation_state << endl;
	cout << "current scale: " << generationProperties->fractal_scale << endl;
	cout << "bias coefficient: " << generationProperties->bias_coefficient << endl;
	generationProperties->smooth_render ? cout << "smooth rendering enabled" << endl : cout << "smooth rendering disabled" << endl;
	generationProperties->randomize_lightness ? cout << "lightness randomization enabled" << endl : cout << "lightness randomization disabled" << endl;
	generationProperties->randomize_alpha ? cout << "alpha randomization enabled (" << generationProperties->alpha_min << ", " << generationProperties->alpha_max << ")" << endl : cout << "alpha randomization disabled" << endl;
	generationProperties->refresh_enabled ? cout << "refresh mode enabled (" << generationProperties->refresh_value << ")" << endl : cout << "refresh mode disabled" << endl;
	generationProperties->two_dimensional ? cout << "2D mode enabled" << endl : cout << "2D mode disabled" << endl;
	generationProperties->scale_matrices ? cout << "scale matrices enabled" << endl : cout << "scale matrices disabled" << endl;
	if (generationProperties->inverted)
		cout << "inverted colors" << endl;
	//cout << "geometry draw type: " << getStringFromGeometryType(sm.geo_type) << endl;
	cout << "lighting mode: " << getStringFromLightingMode(generationProperties->lm) << endl;
	cout << "front geometry matrix type: " << getStringFromGeometryType(geo_type_front) << endl;
	cout << "back geometry matrix type: " << getStringFromGeometryType(geo_type_back) << endl;
	cout << "matrix geometry coefficient: " << generationProperties->matrix_geometry_coefficient << endl;
	cout << "matrix geometry map: " << endl;
	for (const auto &geo_pair : generationProperties->matrix_geometry_weights)
	{
		cout << geo_pair.first << ": " << geo_pair.second << endl;
	}

	cout << "-----------------------------------------------" << endl;
	cout << generationProperties->toString() << endl;
	cout << "-----------------------------------------------" << endl;
}

vector<float> ArtForm::getPalettePoints()
{
	float swatch_height = 2.0f / colors_front.size();

	vector<float> point_data;

	for (int i = 0; i < colors_front.size(); i++)
	{
		vec4 current_color_front = colors_front.at(i);
		vec4 current_color_back = colors_back.at(i);
		vec4 current_color_interpolated = (current_color_front * generationProperties->interpolation_state) + (current_color_back * (1.0f - generationProperties->interpolation_state));

		float top_height = 1.0f - (float(i) * swatch_height);
		float bottom_height = 1.0f - ((i * swatch_height) + swatch_height);
		float swatch_width = 0.05f;

		vector<vec2> front_points;
		float front_left = 1.0f - (swatch_width * 3.0f);
		float front_right = front_left + swatch_width;
		front_points.push_back(vec2(front_left, top_height)); //front top left
		front_points.push_back(vec2(front_right, top_height)); //front top right
		front_points.push_back(vec2(front_right, bottom_height)); // front bottom right

		front_points.push_back(vec2(front_right, bottom_height)); // front bottom right
		front_points.push_back(vec2(front_left, bottom_height)); // front bottom left
		front_points.push_back(vec2(front_left, top_height)); //front top left

		vector<vec2> interpolated_points;
		float interpolated_left = front_right;
		float interpolated_right = interpolated_left + swatch_width;
		interpolated_points.push_back(vec2(interpolated_left, top_height)); //interpolated top left
		interpolated_points.push_back(vec2(interpolated_right, top_height)); //interpolated top right
		interpolated_points.push_back(vec2(interpolated_right, bottom_height)); // interpolated bottom right

		interpolated_points.push_back(vec2(interpolated_right, bottom_height)); // interpolated bottom right
		interpolated_points.push_back(vec2(interpolated_left, bottom_height)); // interpolated bottom left
		interpolated_points.push_back(vec2(interpolated_left, top_height)); //interpolated top left

		vector<vec2> back_points;
		float back_left = interpolated_right;
		float back_right = back_left + swatch_width;
		back_points.push_back(vec2(back_left, top_height)); //back top left
		back_points.push_back(vec2(back_right, top_height)); //back top right
		back_points.push_back(vec2(back_right, bottom_height)); // back bottom right

		back_points.push_back(vec2(back_right, bottom_height)); // back bottom right
		back_points.push_back(vec2(back_left, bottom_height)); // back bottom left
		back_points.push_back(vec2(back_left, top_height)); //back top left
	}

	return point_data;
}

void ArtForm::cycleGeometryType()
{
	if (generationProperties->point_sequence_index == GEOMETRY_ENUM_COUNT)
		generationProperties->point_sequence_index = 0;

	else generationProperties->point_sequence_index++;

	generationProperties->use_point_sequence = GEOMETRY_ENUM_COUNT != generationProperties->point_sequence_index;

	if (generationProperties->use_point_sequence)
	{
		generationProperties->setPointSequenceGeometry(generationProperties->point_sequence_index, randomGenerator);
	}

	cout << "point sequence index: " << generationProperties->point_sequence_index << endl;
}

void ArtForm::setBackgroundColorIndex(int index)
{
	if (index < colors_front.size())
	{
		generationProperties->background_front_index = index;
		generationProperties->background_back_index = index;
		// updateBackground();
	}
}

string ArtForm::getStringFromGeometryType(geometry_type gt) const
{
	switch (gt)
	{
	case CUBOID: return "cuboid";
	case CUBE: return "cube";
	case TETRAHEDRON: return "tetrahedron";
	case OCTAHEDRON: return "octahedron";
	case DODECAHEDRON: return "dodecahedron";
	case ICOSAHEDRON: return "icosahedron";
	//case LOADED_SEQUENCE: return loaded_sequences.at(current_sequence).first;
	case GEOMETRY_TYPE_SIZE: return "points";
	default: return "unknown type";
	}
}

//TODO refactor
void ArtForm::generateLights()
{
	light_indices.clear();
	int light_index_spacing = generationProperties->num_points / generationProperties->num_lights;

	for (int i = 0; i < generationProperties->num_lights; i++)
	{
		light_indices.push_back(i * light_index_spacing);
	}
}

void ArtForm::newColors()
{
	seed_color_front = randomGenerator->getRandomVec4FromColorRanges(
		0.0f, 1.0f,		// red range
		0.0f, 1.0f,		// green range
		0.0f, 1.0f,		// blue range
		generationProperties->alpha_min, generationProperties->alpha_max		// alpha range
	);

	seed_color_back = randomGenerator->getRandomVec4FromColorRanges(
		0.0f, 1.0f,		// red range
		0.0f, 1.0f,		// green range
		0.0f, 1.0f,		// blue range
		generationProperties->alpha_min, generationProperties->alpha_max		// alpha range
	);

	colors_front = generateColorVector(seed_color_front, generationProperties->palette_front, matrices_front.size(), generationProperties->random_palette_front);
	colors_back = generateColorVector(seed_color_back, generationProperties->palette_back, matrices_front.size(), generationProperties->random_palette_back);

	// updateBackground();
}

void ArtForm::cycleColorPalette()
{
	// palettes separated in case these change independently at some point
	generationProperties->palette_front = generationProperties->palette_front == DEFAULT_COLOR_PALETTE ? color_palette(0) : color_palette(int(generationProperties->palette_front) + 1);
	generationProperties->palette_back = generationProperties->palette_back == DEFAULT_COLOR_PALETTE ? color_palette(0) : color_palette(int(generationProperties->palette_back) + 1);
}
