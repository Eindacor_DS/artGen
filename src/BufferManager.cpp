#include "BufferManager.h"

BufferManager::BufferManager(const shared_ptr<ogl_context>& _context)
{
	context = _context;
}

void BufferManager::deleteBuffers()
{
	glDeleteVertexArrays(1, &verticesVAO);
	glDeleteBuffers(1, &verticesVBO);
	glDeleteBuffers(1, &lineIndices);
	glDeleteBuffers(1, &triangleIndices);
}
