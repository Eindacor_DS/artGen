#include "InputTracker.h"

InputTracker::InputTracker(const shared_ptr<ogl_context>& context)
{
	keyHandler = shared_ptr<key_handler>(new key_handler(context));
}

boolean InputTracker::isPressed(int key, bool hold)
{
	return keyHandler->checkPress(key, hold);
}

boolean InputTracker::shiftIsHeld()
{
	return keyHandler->checkShiftHold();
}

boolean InputTracker::altIsHeld()
{
	return keyHandler->checkAltHold();
}

boolean InputTracker::controlIsHeld()
{
	return keyHandler->checkCtrlHold();
}