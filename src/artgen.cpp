#include "artgen.h"

bool getYesOrNo(string prompt, bool def)
{
	string response;
	std::cout << prompt + ": ";
	std::getline(std::cin, response);
	std::cout << std::endl;
	if (response == "" || response == "\n")
		return def;

	else return response == "y" || response == "yes" || response == "true";
}

void getSettings(shared_ptr<GenerationProperties>& generationProperties)
{
	bool use_defaults = getYesOrNo("use default settings?", true);
	if (use_defaults)
		return;

	string seed;
	std::cout << "enter seed: ";
	std::getline(std::cin, seed);
	std::cout << std::endl;
	seed.erase(std::remove(seed.begin(), seed.end(), '\n'), seed.end());
	generationProperties->base_seed = seed;

	string point_count;
	std::cout << "point count: ";
	std::getline(std::cin, point_count);
	std::cout << std::endl;
	generationProperties->num_points = (point_count == "" || point_count == "\n" || std::stoi(point_count) <= 0) ? 10000 : std::stoi(point_count);

	string window_width_input;
	std::cout << "window width: ";
	std::getline(std::cin, window_width_input);
	std::cout << std::endl;
	int window_width = (window_width_input == "" || window_width_input == "\n") ? 1024 : std::stoi(window_width_input);
	generationProperties->window_width = glm::clamp(window_width, 300, 4096);

	string window_height_input;
	std::cout << "window height: ";
	std::getline(std::cin, window_height_input);
	std::cout << std::endl;
	int window_height = (window_height_input == "" || window_height_input == "\n") ? 1024 : std::stoi(window_height_input);
	generationProperties->window_height = glm::clamp(window_height, 300, 4096);
}

int main()
{
	shared_ptr<GenerationProperties> generationProperties(new GenerationProperties());
	getSettings(generationProperties);

	if (generationProperties->base_seed.size() == 0)
	{
		generationProperties->base_seed = RandomGenerator::getRandomString(32);
	}

	shared_ptr<ogl_context> context;

	try
	{
		context = shared_ptr<ogl_context>(new ogl_context("Fractal Generator", "VertexShader.glsl", "PixelShader.glsl", generationProperties->window_width, generationProperties->window_height, false));
	}
	catch (jep::ogl_context_exception e)
	{
		std::cout << e.what();
	}

	shared_ptr<InputTracker> inputTracker(new InputTracker(context));
	shared_ptr<BufferManager> bufferManager(new BufferManager(context));
	shared_ptr<Bufferer> bufferer(new Bufferer(context, bufferManager));

	shared_ptr<Camera> camera(new Camera(context, inputTracker));

	shared_ptr<RandomGenerator> randomGenerator(new RandomGenerator(generationProperties->base_seed));

	generationProperties->randomize(randomGenerator);

	shared_ptr<ColorManager> colorManager(new ColorManager(randomGenerator));

	shared_ptr<ArtForm> artForm(new ArtForm(context, colorManager, bufferer, randomGenerator, generationProperties));
	shared_ptr<Renderer> renderer(new Renderer(context, camera, colorManager, bufferManager, randomGenerator, generationProperties));
	
	/*jep::obj_contents torus("torus.obj");
	jep::obj_contents helix("helix.obj");
	jep::obj_contents sphere("sphere.obj");
	generator->loadPointSequence("torus", torus.getAllVerticesOfAllMeshes());
	generator->loadPointSequence("helix", helix.getAllVerticesOfAllMeshes());
	generator->loadPointSequence("sphere", sphere.getAllVerticesOfAllMeshes());*/

	glfwSetTime(0);
	float render_fps = 60.0f;
	bool finished = false;
	clock_t start = clock();
	bool paused = false;
	bool pause_on_swap = false;
	bool growth_paused = false;
	bool recording = false;
	int gif_frame_count = 150;
	int current_gif_frame = 0;

	artForm->printContext();

	bool errors_found = false;
	try
	{
		while (!finished)
		{
			if (glfwGetTime() > 1.0f / render_fps && !errors_found)
			{
				if ((clock() - start) / CLOCKS_PER_SEC > 2.0f)
					start = clock();

				glfwPollEvents();
				context->clearBuffers();

				if (recording)
				{
					batchRender(artForm, context, BMP, 4, 1, 1, 720, false, camera, renderer);
					current_gif_frame++;

					if (current_gif_frame == gif_frame_count)
					{
						recording = false;
						current_gif_frame = 0;
					}
				}

				if (inputTracker->isPressed(GLFW_KEY_SLASH, false))
				{
					pause_on_swap = !pause_on_swap;
				}

				if (pause_on_swap && (artForm->getInterpolationState() < 0.0001f || artForm->getInterpolationState() > 0.9999f))
				{
					paused = true;
					pause_on_swap = false;
				}

				if (!paused)
				{
					artForm->tickAnimation();
				}

				if (inputTracker->isPressed(GLFW_KEY_5, false))
				{
					growth_paused = !growth_paused;
				}

				renderer->update(inputTracker, artForm);
				camera->update(inputTracker);
				artForm->update(inputTracker);
				renderer->render(artForm);	

				//TODO see why this only works when include_hold is enabled
				if (inputTracker->isPressed(GLFW_KEY_ESCAPE, true))
				{
					finished = true;
				}

				if (inputTracker->isPressed(GLFW_KEY_T, false))
				{
					paused = !paused;
				}

				/*GLenum err;
				while ((err = glGetError()) != GL_NO_ERROR)
				{
					std::cout << "OpenGL error: " << err << endl;
					errors_found = true;
				}*/

				context->swapBuffers();

				if (inputTracker->isPressed(GLFW_KEY_X, false))
				{
					capture(inputTracker, artForm, context, camera, gif_frame_count, recording, paused, renderer);
				}

				if (inputTracker->isPressed(GLFW_KEY_SPACE, false))
				{
					randomGenerator->seed(RandomGenerator::getRandomString(32));
					generationProperties->randomize(randomGenerator);
					colorManager.reset();
					colorManager = shared_ptr<ColorManager>(new ColorManager(randomGenerator));
					artForm.reset();
					artForm = shared_ptr<ArtForm>(new ArtForm(context, colorManager, bufferer, randomGenerator, generationProperties));
					renderer.reset();
					renderer = shared_ptr<Renderer>(new Renderer(context, camera, colorManager, bufferManager, randomGenerator, generationProperties));
					camera->setDefaults();
					paused = false;
					growth_paused = false;
				}

				if (inputTracker->isPressed(GLFW_KEY_R, false))
				{
					randomGenerator->seed(generationProperties->base_seed);
					colorManager.reset();
					colorManager = shared_ptr<ColorManager>(new ColorManager(randomGenerator));
					artForm.reset();
					artForm = shared_ptr<ArtForm>(new ArtForm(context, colorManager, bufferer, randomGenerator, generationProperties));
					renderer.reset();
					renderer = shared_ptr<Renderer>(new Renderer(context, camera, colorManager, bufferManager, randomGenerator, generationProperties));
					camera->setDefaults();
					paused = false;
					growth_paused = false;
				}

				glfwSetTime(0.0f);
			}
		}
	}
	catch (std::exception e)
	{
		cout << e.what() << std::endl;
		char input;
		std::cin >> input;
	}

	return 0;
}

//void refresh(settings_manager &settings, boost::shared_ptr<jep::ogl_context> &context, boost::shared_ptr<ArtForm> &generator, boost::shared_ptr<Camera> &camera)
//{
//	shared_ptr<ArtForm> new_generator(new ArtForm(GenerationProperties->base_seed, context, GenerationProperties->num_points));
//	generator = new_generator;
//	generator->printContext();
//	/*generator->loadPointSequence("torus", torus.getAllVerticesOfAllMeshes());
//	generator->loadPointSequence("helix", helix.getAllVerticesOfAllMeshes());
//	generator->loadPointSequence("sphere", sphere.getAllVerticesOfAllMeshes());*/
//	camera->setDefaults();
//
//	context->setUniform1i("override_line_color_enabled", 0);
//	context->setUniform1i("override_triangle_color_enabled", 0);
//	context->setUniform1i("override_point_color_enabled", 0);
//}

//void newGen(settings_manager &settings, boost::shared_ptr<InputTracker> &inputTracker, RandomGenerator &mc, boost::shared_ptr<jep::ogl_context> &context, boost::shared_ptr<ArtForm> &generator, boost::shared_ptr<Camera> &camera)
//{
//	GenerationProperties->base_seed = "";
//
//	if (inputTracker->shiftIsHeld())
//	{
//		string seed;
//		cout << "enter seed: ";
//		std::getline(std::cin, seed);
//		cout << endl;
//		seed.erase(std::remove(seed.begin(), seed.end(), '\n'), seed.end());
//		GenerationProperties->base_seed = seed;
//	}
//
//	if (GenerationProperties->base_seed.size() == 0)
//		GenerationProperties->base_seed = mc.generateAlphanumericString(32);
//
//	shared_ptr<ArtForm> new_generator(new ArtForm(GenerationProperties->base_seed, context, GenerationProperties->num_points));
//	generator = new_generator;
//	generator->printContext();
//	/*generator->loadPointSequence("torus", torus.getAllVerticesOfAllMeshes());
//	generator->loadPointSequence("helix", helix.getAllVerticesOfAllMeshes());
//	generator->loadPointSequence("sphere", sphere.getAllVerticesOfAllMeshes());*/
//	camera->setDefaults();
//
//	context->setUniform1i("override_light_color_enabled", 0);
//	context->setUniform1i("override_line_color_enabled", 0);
//	context->setUniform1i("override_triangle_color_enabled", 0);
//	context->setUniform1i("override_point_color_enabled", 0);
//}

void capture(const shared_ptr<InputTracker> &inputTracker, const shared_ptr<ArtForm> &artForm, const shared_ptr<jep::ogl_context> &context, const shared_ptr<Camera> &camera, int &gif_frame_count, bool &recording, bool &paused, const shared_ptr<Renderer>& renderer)
{
	if (inputTracker->shiftIsHeld())
	{
		string x_quadrants_input;
		std::cout << "x quadrants: ";
		std::getline(std::cin, x_quadrants_input);
		cout << endl;
		int x_quadrants = (x_quadrants_input == "" || x_quadrants_input == "\n") ? 1 : std::stoi(x_quadrants_input);
		x_quadrants = glm::clamp(x_quadrants, 1, 100);

		string y_quadrants_input;
		cout << "y quadrants: ";
		std::getline(std::cin, y_quadrants_input);
		cout << endl;
		int y_quadrants = (y_quadrants_input == "" || y_quadrants_input == "\n") ? 1 : std::stoi(y_quadrants_input);
		y_quadrants = glm::clamp(y_quadrants, 1, 100);

		string quadrant_size_input;
		cout << "quadrant size: ";
		std::getline(std::cin, quadrant_size_input);
		cout << endl;
		int quadrant_size = (quadrant_size_input == "" || quadrant_size_input == "\n") ? 1024 : std::stoi(quadrant_size_input);
		quadrant_size = glm::clamp(quadrant_size, 128, 2048);

		bool mix_background = getYesOrNo("varied background?", false);

		batchRender(artForm, context, BMP, 6, x_quadrants, y_quadrants, quadrant_size, mix_background, camera, renderer);
	}
	else if (inputTracker->controlIsHeld())
	{
		bool mix_background = getYesOrNo("varied background?", false);

		batchRender(artForm, context, BMP, 6, 4, 4, 2250, mix_background, camera, renderer); // 30x30
		batchRender(artForm, context, BMP, 6, 4, 4, 1800, mix_background, camera, renderer);	// 24x24
		batchRender(artForm, context, BMP, 6, 2, 2, 2400, mix_background, camera, renderer);	// 16x16
		batchRender(artForm, context, BMP, 6, 2, 2, 1800, mix_background, camera, renderer);	// 12x12
		batchRender(artForm, context, BMP, 6, 1, 1, 2048, mix_background, camera, renderer);	// preview
	}
	else if (inputTracker->altIsHeld())
	{
		string frame_record_count;
		cout << "frames to record: ";
		std::getline(std::cin, frame_record_count);
		cout << endl;
		gif_frame_count = (frame_record_count == "" || frame_record_count == "\n") ? 150 : std::stoi(frame_record_count);
		gif_frame_count = glm::clamp(gif_frame_count, 30, 900);

		recording = true;
		paused = false;
	}
	else
	{
		//saveImage(*generator, context, JPG);
		//saveImage(*generator, context, PNG);
		saveImage(artForm, context, BMP, 6, camera, renderer);
		//saveImage(*generator, context, TIFF);
	}
}
