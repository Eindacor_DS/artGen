#include "Bufferer.h"

Bufferer::Bufferer(const shared_ptr<ogl_context>& _context, const shared_ptr<BufferManager>& _bufferManager)
{
	context = _context;
	bufferManager = _bufferManager;
}

void Bufferer::bufferData(const vector<float> &vertexData, const vector<unsigned short> &lineIndices, const vector<unsigned short> &triangleIndices)
{
	if (initialized)
	{
		bufferManager->deleteBuffers();
	}

	updateCount(vertexData, vertexCount);
	updateCount(lineIndices, lineIndexCount);
	updateCount(triangleIndices, triangleIndexCount);

	// create/bind Vertex Array Object
	glGenVertexArrays(1, bufferManager->getVerticesVAOPointer());
	glBindVertexArray(bufferManager->getVerticesVAO());

	// create/bind Vertex Buffer Object
	//TODO below shoudl be address of the VBO glint, not just the glint
	glGenBuffers(1, bufferManager->getVerticesVBOPointer());
	glBindBuffer(GL_ARRAY_BUFFER, bufferManager->getVerticesVBO());
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * vertexCount, &vertexData[0], GL_STATIC_DRAW);

	// stride is the total size of each vertex's attribute data (position + color + size)
	// change this to 7 for triangle bug
	int stride = vertexSize * sizeof(float);

	// load position data
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, stride, (void*)0);

	// load color data
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, stride, (void*)(4 * sizeof(float)));

	// load point size
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, stride, (void*)(8 * sizeof(float)));

	glGenBuffers(1, bufferManager->getLineIndicesPointer());
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferManager->getLineIndices());
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, lineIndexCount * sizeof(unsigned short), &lineIndices[0], GL_STATIC_DRAW);

	glGenBuffers(1, bufferManager->getTriangleIndicesPointer());
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferManager->getTriangleIndices());
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, triangleIndexCount * sizeof(unsigned short), &triangleIndices[0], GL_STATIC_DRAW);

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	initialized = true;
}

template <typename T> boolean Bufferer::updateCount(const vector<T>& collection, int& count)
{
	if (collection.size() != count)
	{
		count = collection.size();
		return true;
	}

	return false;
}

void Bufferer::bufferLightData(const vector<float> &vertex_data, const vector<int>& light_indices)
{
	for (int i = 0; i < LIGHT_COUNT; i++)
	{
		if (i < light_indices.size())
		{
			int light_index = light_indices.at(i);
			int data_index = light_index * vertexSize;

			vec4 light_position(vertex_data.at(data_index), vertex_data.at(data_index + 1), vertex_data.at(data_index + 2), 1.0f);
			vec4 light_color(vertex_data.at(data_index + 4), vertex_data.at(data_index + 5), vertex_data.at(data_index + 6), 1.0f);

			light_positions[i] = light_position;
			light_colors[i] = light_color;
		}
		else
		{
			light_positions[i] = vec4(0.0f);
			light_colors[i] = vec4(0.0f);
		}
	}

	context->setUniform4fv("light_positions", LIGHT_COUNT, light_positions[0]);
	context->setUniform4fv("light_colors", LIGHT_COUNT, light_colors[0]);
}