#pragma once

#include "header.h"

using jep::key_handler;

class InputTracker 
{
public:
	InputTracker(const shared_ptr<ogl_context>& context);
	boolean isPressed(int key, bool hold);
	boolean shiftIsHeld();
	boolean altIsHeld();
	boolean controlIsHeld();
	shared_ptr<key_handler> getKeyHandler() const { return keyHandler; }

private:
	shared_ptr<key_handler> keyHandler;
};