#pragma once

#include "header.h"

class BufferManager
{
public:
	BufferManager(const shared_ptr<ogl_context>& context);

	void deleteBuffers();

	~BufferManager() {
		deleteBuffers();
	}

	GLuint getVerticesVBO() { return verticesVBO; }
	GLuint getVerticesVAO() { return verticesVAO; }

	GLuint* getVerticesVBOPointer() { return &verticesVBO; }
	GLuint* getVerticesVAOPointer() { return &verticesVAO; }

	GLuint getLineIndices() { return lineIndices; }
	GLuint getTriangleIndices() { return triangleIndices; }

	GLuint* getLineIndicesPointer() { return &lineIndices; }
	GLuint* getTriangleIndicesPointer() { return &triangleIndices; }

private:
	shared_ptr<ogl_context> context;

	GLuint verticesVBO;
	GLuint verticesVAO;

	GLuint lineIndices;
	GLuint triangleIndices;
};