#pragma once

#include "header.h"
#include "InputTracker.h"

class Camera
{
public:
	Camera(const shared_ptr<ogl_context>& _context, const shared_ptr<InputTracker>& _inputTracker);

	shared_ptr<ogl_camera_flying> getCamera() { return camera; }

	void update(const shared_ptr<InputTracker>& inputTracker);

	const glm::vec3 getFocus() const { return camera->getFocus(); }
	const glm::vec3 getPosition() const { return camera->getPosition(); }
	int getDOFPasses() { return depthOfFieldPasses; }
	vec3 getFocalPoint() const { return focalPoint; }

	glm::mat4 getMVP();

	boolean dofIsEnabled() { return depthOfFieldEnabled; }

	vector<glm::mat4> getDOFMVPs();

	void setDefaults();

private:
	shared_ptr<ogl_camera_flying> camera;
	shared_ptr<InputTracker> inputTracker;
	shared_ptr<ogl_context> context;

	float aperture;
	bool depthOfFieldEnabled;
	int depthOfFieldPasses;
	float fieldOfView;
	float eyeLevel;

	vec3 focalPoint;

	
};