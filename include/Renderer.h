#pragma once

#include "header.h"
#include "Camera.h"
#include "ArtForm.h"
#include "GenerationProperties.h"
#include "ColorManager.h"
#include "BufferManager.h"

class Renderer
{
public:
	Renderer(
		const shared_ptr<ogl_context>& context, 
		const shared_ptr<Camera>& camera, 
		const shared_ptr<ColorManager>& colorManager,
		const shared_ptr<BufferManager>& bufferManager,
		const shared_ptr<RandomGenerator>& randomGenerator,
		const shared_ptr<GenerationProperties>& generationProperties
	);

	void render(const shared_ptr<ArtForm>& artForm) const;

	void update(const shared_ptr<InputTracker>& inputTracker, const shared_ptr<ArtForm>& artForm);

	int getMaxPointSize() const { return maxPointSize; }

	void cycleBackgroundColorIndex(const shared_ptr<ArtForm>& artForm);

private:
	// -3 = no override, -2 = black, -1 = white, 0 - n for each interpolated matrix_color
	int light_color_override_index = -3;
	int line_color_override_index = -3;
	int triangle_color_override_index = -3;
	int point_color_override_index = -3;
	int maxPointSize;

	float point_size_modifier = 1.0f;

	vec4 background_color;

	mat4 fractal_scale_matrix;

	void draw(const shared_ptr<ArtForm>& artForm) const;

	void drawWithDOF(const shared_ptr<ArtForm>& artForm, const shared_ptr<Camera>& camera) const;

	void drawVertices(const shared_ptr<ArtForm>& artForm) const;

	void drawLines(const shared_ptr<ArtForm>& artForm) const;

	void drawTriangles(const shared_ptr<ArtForm>& artForm) const;

	void adjustLineWidth(boolean increase);

	void updateBackground(const shared_ptr<ArtForm>& artForm) const;

	void invertColors(const shared_ptr<ArtForm>& artForm);

	void updateLightColorOverride(const shared_ptr<ArtForm>& artForm);

	void updateLineColorOverride(const shared_ptr<ArtForm>& artForm);

	void updateTriangleColorOverride(const shared_ptr<ArtForm>& artForm);

	void updatePointColorOverride(const shared_ptr<ArtForm>& artForm);

	void applyBackground(const int & num_samples);

	void adjustBackgroundBrightness(float adjustment);

	void cycleLightColorOverride(const shared_ptr<ArtForm>& artForm);

	void cycleLineColorOverride(const shared_ptr<ArtForm>& artForm);

	void cycleTriangleColorOverride(const shared_ptr<ArtForm>& artForm);

	void cyclePointColorOverride(const shared_ptr<ArtForm>& artForm);

	vec4 getSampleColor(const int & samples, const vector<vec4>& color_pool) const;

	shared_ptr<ogl_context> context;
	shared_ptr<Camera> camera;
	shared_ptr<GenerationProperties> generationProperties;
	shared_ptr<ColorManager> colorManager;
	shared_ptr<ArtForm> artForm;
	shared_ptr<BufferManager> bufferManager;
	shared_ptr<RandomGenerator> randomGenerator;
};