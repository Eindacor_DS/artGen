#pragma once

#include "header.h"
#include "ArtForm.h"
#include "Camera.h"
#include "Renderer.h"

#using <system.drawing.dll>
using namespace System;
using namespace System::Drawing;
using namespace System::Drawing::Imaging;

bool saveImage(
	const shared_ptr<ArtForm> &artForm, 
	const shared_ptr<ogl_context> &context, 
	image_extension ie, 
	int multisample_count, 
	const shared_ptr<Camera> &camera, 
	const shared_ptr<Renderer>& renderer);

bool batchRender(
	const shared_ptr<ArtForm>& artForm,
	const shared_ptr<ogl_context> &context, 
	image_extension ie, 
	int multisample_count, 
	int x_count, 
	int y_count,
	int quadrant_size,
	bool mix_background,
	const shared_ptr<Camera> &camera, 
	const shared_ptr<Renderer>& renderer);

string paddedValue(unsigned int value, unsigned short total_digits);
GLint glExtCheckFramebufferStatus(char *errorMessage);