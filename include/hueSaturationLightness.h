#pragma once

#include "header.h"

class hueSaturationLightness
{
public:
	int H;
	float S;
	float L;

	hueSaturationLightness(int h, float s, float l)
	{
		H = h;
		S = s;
		L = l;
	}

	bool operator==(const hueSaturationLightness& other);
	bool operator!=(const hueSaturationLightness& other) { return !(*this == other); }

	string to_string() const { return std::to_string(H) + ", " + std::to_string(S) + ", " + std::to_string(L); }
};