#pragma once

#include "header.h"
#include "RandomGenerator.h"
#include "ColorManager.h"
#include "GenerationProperties.h"
#include "geometry_generator.h"
#include "InputTracker.h"
#include "Camera.h"
#include "Bufferer.h"

/*
The ArtForm class is responsible for generating vertex position and geometry data
*/
class ArtForm
{
public:
	ArtForm(
		const shared_ptr<jep::ogl_context> &context,
		const shared_ptr<ColorManager> &colorManager,
		const shared_ptr<Bufferer> &bufferer,
		const shared_ptr<RandomGenerator>& _randomGenerator,
		const shared_ptr<GenerationProperties>& _generationProperties);

	void setMatrices();

	void generateFractal();
	void generateFractalFromPointSequence();
	void generateFractalWithRefresh();
	void generateFractalFromPointSequenceWithRefresh();

	void updateIndexCountsAndBufferData(std::vector<unsigned short> &line_indices_to_buffer, std::vector<unsigned short> &triangle_indices_to_buffer, std::vector<float> &points);

	void renderFractal(const int &image_width, const int &image_height, const int &matrix_sequence_count);

	//vector<mat4> generateMatrixSequence(const vector<int> &matrix_indices) const;
	vector<mat4> generateMatrixSequence(const int &sequence_size) const;

	void update(const shared_ptr<InputTracker> &keys);
	
	// keeps track of how many indices are called by draw command, set by geometry index pattern generated in geometry_generator.cpp
	int point_index_count;
	int line_index_count;
	int triangle_index_count;

	void regenerateFractal();

	void printMatrices() const;

	void tickAnimation();
	void swapMatrices();
	string getGenerationSeed();
	void loadPointSequence(string name, const vector<vec4> &sequence);
	void printContext();
	void cycleGeometryType();
	void setBackgroundColorIndex(int index);
	int getBackgroundColorIndex() const { return generationProperties->background_front_index; }

	float getAverageDelta() const { return average_delta; }

	signed int getGeneration() const { return generation; }

	bool getShowGrowth() const { return show_growth; }
	int getVerticesToRender() const { return vertices_to_render; }

	vec3 getCentroid() { return centroid; }

	vector<vec4> getColorsFront() const { return colors_front; }
	vector<vec4> getColorsBack() const { return colors_back; }
	float getInterpolationState() const { return generationProperties->interpolation_state; }

	void setTwoDimensional(bool b) { generationProperties->two_dimensional = b; }
	string getStringFromGeometryType(geometry_type gt) const;

	string getBaseSeed() { return randomGenerator->getSeed(); }

	vector < std::pair<string, vector<vec4> > > getLoadedSequences() const { return loaded_sequences; }

	int getVerticesToRenderCount() const { return show_growth ? glm::clamp(vertices_to_render, 0, vertex_count) : vertex_count; }
	int getLineVerticesToRenderCount() const { return show_growth ? glm::clamp(vertices_to_render, 0, line_index_count) : line_index_count; }
	int getTriangleVerticesToRenderCount() const { return show_growth ? glm::clamp(vertices_to_render, 0, triangle_index_count) : triangle_index_count; }

	void newColors();

private:
	vector<unsigned int> matrix_sequence_front;
	vector<unsigned int> matrix_sequence_back;
	vector< std::pair<string, mat4> > matrices_front;
	vector< std::pair<string, mat4> > matrices_back;
	vector<vec4> colors_front;
	vector<vec4> colors_back;
	vec4 seed_color_front;
	vec4 seed_color_back;
	vector<float> sizes_front;
	vector<float> sizes_back;
	geometry_type geo_type_front = GEOMETRY_TYPE_SIZE;
	geometry_type geo_type_back = GEOMETRY_TYPE_SIZE;
	shared_ptr<RandomGenerator> randomGenerator;
	shared_ptr<GenerationProperties> generationProperties;
	GeometryGenerator gm;
	float average_delta, max_x, max_y, max_z;
	
	int generation = 0;
	bool matrix_geometry_uses_solid_geometry = false;

	unsigned int current_frame = 0;
	unsigned int frame_increment = 1;
	bool reverse_growth = false;
	int vertices_to_render = 0;
	bool show_growth = false;
	
	// current gen parameters	
	vec4 origin = vec4(0.0f, 0.0f, 0.0f, 1.0f);	
	// vector instead of a map to make cycling easy
	vector < std::pair<string, vector<vec4> > > loaded_sequences;

	vector<int> light_indices;
	vec3 centroid;

	//TODO explain 9
	unsigned short vertex_size;
	int vertex_count;

	shared_ptr<ogl_context> context;
	shared_ptr<ColorManager> colorManager;
	shared_ptr<Bufferer> bufferer;

	void addNewPointAndIterate(
		vec4 &starting_point,
		vec4 &starting_color,
		float &starting_size,
		int matrix_index_front,
		int matrix_index_back,
		vector<float> &points);

	void addPointSequenceAndIterate(
		mat4 &origin_matrix,
		vec4 &starting_color,
		float &starting_size,
		int matrix_index_front,
		int matrix_index_back,
		vector<float> &points,
		vector<unsigned short> &line_indices,
		vector<unsigned short> &triangle_indices,
		int &current_sequence_index_lines,
		int &current_sequence_index_triangles);

	void addNewPoint(
		const vec4 &point,
		const vec4 &color,
		const float &size,
		vector<float> &points);

	vector< std::pair<string, mat4> > generateMatrixVector(const int &count, geometry_type &geo_type);

	vector<vec4> generateColorVector(const vec4 &seed, color_palette palette, const int &count, color_palette &random_selection) const;

	vector<float> generateSizeVector(const int &count) const;

	vector<float> getPalettePoints();

	void generateLights();
	void cycleColorPalette();
};