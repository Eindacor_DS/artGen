#pragma once

#include "header.h"
#include "RandomGenerator.h"
#include "screencap.h"
#include "geometry_generator.h"
#include "GenerationProperties.h"
#include "InputTracker.h"
#include "Camera.h"
#include "ArtForm.h"
#include "ColorManager.h"
#include "Renderer.h"

void capture(const shared_ptr<InputTracker> &inputTracker, const shared_ptr<ArtForm> &artForm, const shared_ptr<jep::ogl_context> &context, const shared_ptr<Camera> &camera, int &gif_frame_count, bool &recording, bool &paused, const shared_ptr<Renderer>& renderer);