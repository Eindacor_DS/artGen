#pragma once

#include "header.h"
#include "BufferManager.h"

/*
Buffers data to the GPU
*/
class Bufferer
{
public:
	Bufferer(
		const shared_ptr<ogl_context>& context,
		const shared_ptr<BufferManager>& bufferManager
		);

	void bufferData(const vector<float>& vertex_data, const vector<unsigned short>& line_indices_to_buffer, const vector<unsigned short>& triangle_indices_to_buffer);

	template <typename T> boolean updateCount (const vector<T>& collection, int& count);

	void bufferLightData(const vector<float>& vertex_data, const vector<int>& light_indices);

	int getVertexSize() { return vertexSize; }

private:
	//vec4 position, vec4 color, float point size
	int vertexSize = 9;

	bool initialized = false;

	int vertexCount = 0;
	int lineIndexCount = 0;
	int triangleIndexCount = 0;

	shared_ptr<ogl_context> context;
	shared_ptr<BufferManager> bufferManager;

	vec4 light_positions[LIGHT_COUNT];
	vec4 light_colors[LIGHT_COUNT];
};